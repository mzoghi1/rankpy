#!/usr/bin/python2
# -*- coding: utf-8 -*-

from numpy import *
set_printoptions(edgeitems=3, infstr='inf', linewidth=9999, nanstr='nan', precision=4, suppress=False, threshold=1000, formatter=None)
from numpy.random import choice
import matplotlib
matplotlib.use('Agg')
from matplotlib.pyplot import *
from matplotlib.colors import hsv_to_rgb
import logging
import os
import sys
from collections import Counter

from rankpy.queries import Queries
from rankpy.queries import find_constant_features, find_nonconstant_query_features, concatenate

from rankpy.models import LambdaMART


# Turn on logging.
logging.basicConfig(format='%(asctime)s : %(message)s', level=logging.INFO)


if __name__ == "__main__":
    dataset = 'YLR2'
    if len(sys.argv) > 1:
        dataset = sys.argv[1]
    if not os.path.isfile(os.environ['HOME']+'/online-learning-data/'+dataset+'/training') or \
       not os.path.isfile(os.environ['HOME']+'/online-learning-data/'+dataset+'/validation') or \
       not os.path.isfile(os.environ['HOME']+'/online-learning-data/'+dataset+'/test'):
        # Load the query datasets.
        training_queries = Queries.load_from_text(os.environ['HOME']+'/online-learning-data/'+dataset+'/train.txt.gz')
        validation_queries = Queries.load_from_text(os.environ['HOME']+'/online-learning-data/'+dataset+'/vali.txt.gz')
        test_queries = Queries.load_from_text(os.environ['HOME']+'/online-learning-data/'+dataset+'/test.txt.gz')
        logging.info('=' * 80)
        # Save them to binary format ...
        training_queries.save(os.environ['HOME']+'/online-learning-data/'+dataset+'/training',separate=True)
        validation_queries.save(os.environ['HOME']+'/online-learning-data/'+dataset+'/validation',separate=True)
        test_queries.save(os.environ['HOME']+'/online-learning-data/'+dataset+'/test',separate=True)

    # ... because loading them will be then faster.
    training_queries = Queries.load(os.environ['HOME']+'/online-learning-data/'+dataset+'/training', mmap='r')
    validation_queries = Queries.load(os.environ['HOME']+'/online-learning-data/'+dataset+'/validation', mmap='r')
    test_queries = Queries.load(os.environ['HOME']+'/online-learning-data/'+dataset+'/test', mmap='r')

    logging.info('=' * 80)

    # Print basic info about query datasets.
    logging.info('Train queries: %s' % training_queries)
    logging.info('Valid queries: %s' % validation_queries)
    logging.info('Test queries: %s' %test_queries)

    logging.info('=' * 80)

    # Sort the values of each query feature and plot them
    queries = concatenate([training_queries,validation_queries,test_queries])
    QF, QFV = queries.get_query_features_and_values()
    nQ = QFV.shape[0]
    nF = QFV.shape[1]
    QFV -= QFV.min(axis=0)
    QFV /= QFV.max(axis=0)
    sQFV = array(QFV)
    sQFV.sort(axis=0)
    fig = figure(10)
    clf()
    ax = fig.add_subplot(111)
    ax.hold('on')
    x = linspace(0,1,sQFV.shape[0])
    for ind,nV in enumerate(sQFV.T):
        # print ind, [.7*float(ind)/nF,1,1], hsv_to_rgb([.7*float(ind)/nF,1.,1.])
        ax.plot(x,nV,color=hsv_to_rgb([.9*float(ind)/nF,1.,1.]))
    ax.grid('on')
    axis([-.01,1.01,-.01,1.01])
    if not os.path.isdir('FIGS'):
        os.makedirs('FIGS')
    savefig('FIGS/normalized_query_feature_values_for_'+dataset+'.pdf',bbox_inches='tight')

    # Separate the continuous variables from the discrete ones
    ctsFs = zeros(nF)
    dscFs = zeros(nF)
    for ind,nV in enumerate(sQFV.T):
        print '(qFeature index, unique values):', (ind, unique(nV).shape[0])
        if unique(nV).shape[0] > min(nQ/10,100):
            ctsFs[ind] = 1
        elif unique(nV).shape[0] <= 100:
            dscFs[ind] = 1
    ctsFs = nonzero(ctsFs)[0]
    dscFs = nonzero(dscFs)[0]
    print 'Continuous features:', ctsFs
    print 'Discrete features', dscFs

    # Create correlation plots for pairs of continuous features
    if len(ctsFs) > 1:
        Inds = arange(nQ)
        chInds = choice(Inds,100,replace=False)
        chInds.sort()
        fig = figure(11)
        clf()
        ax = fig.add_subplot(111)
        ax.hold('on')
        for ind in ctsFs:
            for jnd in ctsFs:
                if jnd <= ind:
                    continue
                # print ind, jnd, [.9*float(ind)/nF,1.,1.-float(jnd)/nF], hsv_to_rgb([.9*float(ind)/nF,1.,1.-.5*jnd/nF])
                ax.scatter(QFV[chInds,ind],QFV[chInds,jnd],s=5,marker='.',edgecolor=hsv_to_rgb([.9*float(ind)/nF,1.,1.-.5*jnd/nF]))
        ax.grid('on')
        m = QFV[ix_(chInds,ctsFs)].min()
        M = QFV[ix_(chInds,ctsFs)].max()
        axis([m-.01,M+.01,m-.01,M+.01])
        savefig('FIGS/normalized_query_feature_correlation_plots_for_continuous_features_of_'+dataset+'.pdf',bbox_inches='tight')

    # Create correlation plots for pairs of discrete features
    if len(dscFs) > 1:
        chInds = choice(Inds,100,replace=False)
        chInds.sort()
        fig = figure(12)
        clf()
        ax = fig.add_subplot(111)
        ax.hold('on')
        for ind in dscFs:
            for jnd in dscFs:
                if jnd <= ind:
                    continue
                # print ind, jnd, [.9*float(ind)/nF,1.,1.-float(jnd)/nF], hsv_to_rgb([.9*float(ind)/nF,1.,1.-.5*jnd/nF])
                iV = QFV[chInds,ind] 
                jV = QFV[chInds,jnd]
                C = Counter(zip(iV,jV))
                ijV,S = zip(*sorted(C.items()))
                iV,jV = zip(*ijV)
                ax.scatter(iV,jV,s=10*array(S),marker='.',edgecolor=hsv_to_rgb([.9*float(ind)/nF,1.,1.-.5*jnd/nF]),
                    facecolor=hsv_to_rgb([.9*float(ind)/nF,1.,1.-.5*jnd/nF]))
        ax.grid('on')
        m = QFV[ix_(chInds,dscFs)].min()
        M = QFV[ix_(chInds,dscFs)].max()
        axis([m-.01,M+.01,m-.01,M+.01])
        savefig('FIGS/normalized_query_feature_correlation_plots_for_discrete_features_of_'+dataset+'.pdf',bbox_inches='tight')



    """ Testing the inequality operations
        V = unique(QFV[:,0])
        V.sort()
        C = Counter(sQFV[:,0])
        print dict(enumerate(sorted(C.items())))
        print "len(queries[queries < {QF[0]:V[6]}]) + len(queries[queries >= {QF[0]:V[6]}]) =", 
        print len(queries[queries < {QF[0]:V[6]}]), '+', len(queries[queries >= {QF[0]:V[6]}]), '=', 
        print len(queries[queries < {QF[0]:V[6]}]) + len(queries[queries >= {QF[0]:V[6]}]), '=?', len(queries), '= len(queries)'
        print "len(queries[queries > {QF[0]:V[6]}]) + len(queries[queries <= {QF[0]:V[6]}]) =", 
        print len(queries[queries > {QF[0]:V[6]}]), '+', len(queries[queries <= {QF[0]:V[6]}]), '=', 
        print len(queries[queries > {QF[0]:V[6]}]) + len(queries[queries <= {QF[0]:V[6]}]), '=?', len(queries), '= len(queries)'
    """




from numpy import *
set_printoptions(edgeitems=3, infstr='inf', linewidth=9999, nanstr='nan', precision=4, suppress=False, threshold=1000, formatter=None)
from numpy.random import multinomial

def tuples2dict(Pairs,Type):
    # Pairs is assumed to be a list of binary tuples
    # D is a dictionary with the first elements of the pairs in Pairs as the keys 
    # and the value of each key is an array consisting of the second elements of the pairs whose first element was the key
    D = {}
    K, V = zip(*Pairs)
    for key in set(K):
        D[key] = array([v for k,v in Pairs if k == key]).astype(Type)
    return D


class MyNestedTree:
    """This is a book-keeping class that keeps track of the tree and its various attributes as it grows."""
    def __init__(self,criterion='mse',n_outputs=1,n_classes=[1],sample_ind_dtype='i,i',Data=None):
        # print "Method: MyNestedTree.__init__()"
        self.criterion=criterion
        self.n_outputs=n_outputs
        self.n_classes=n_classes
        self.sample_ind_dtype = sample_ind_dtype

        self._value_ = {(0,0):None}
        self._impurity_ = {(0,0):None}
        self._children_left_ = {(0,0):None}
        self._children_right_ = {(0,0):None}
        self._feature_ = {(0,0):None}
        self._threshold_ = {(0,0):None}

        self.allNodeConnections = {}
        self.allNodeConnections[(0,0)] = {'parent':None, 'left child':None, 'right child':None,
                                          'value_is_updated':False, 'impurity_is_updated':False}
        self.allData = {}
        if Data is not None:
            self.allData[(0,0)] = Data.copy_values()
        self.real_node_id = {(0,0):(0,0)}

    def keys(self):
        return self.allNodeConnections.keys()

    def data_keys(self):
        return self.allData.keys()

    def sample_value(self,node=None,num_samples=1):
        # print "Method: MyNestedTree.value()"
        rnode = self.real_node_id[tuple(node)]
        Y = self.allData[rnode].Y
        W = self.allData[rnode].sample_weight
        W = W/W.sum()
        Inds = [nonzero(multinomial(1,W))[0] for _ in range(int(num_samples))]
        if num_samples > 1:
            return Y[Inds]
        else:
            return Y[Inds[0]]

    def value(self,node=None):
        # print "Method: MyNestedTree.value()"
        if node is not None:
            rnode = self.real_node_id[tuple(node)]
            if self[rnode]['value_is_updated'] and rnode in self._value_:
                val = self._value_[rnode]
            else:
                Y = self.allData[rnode].Y
                W = self.allData[rnode].sample_weight
                val = (W*Y).sum() / W.sum()
                self._value_[node] = val
                self[rnode]['value_is_updated'] = True
            return val
        else:
            for node in self.allNodeConnections:
                self._value_[node] = self.value(node)
                self[node]['value_is_updated'] = True
            return self._value_

    def impurity(self,node=None):
        # print "Method: MyNestedTree.impurity()"
        if node is not None:
            rnode = self.real_node_id[tuple(node)]
            if self[rnode]['impurity_is_updated'] and rnode in self._impurity_:
                imp = self._impurity_[node]
            else:
                Y = self.allData[rnode].Y
                W = self.allData[rnode].sample_weight
                imp = (W*Y*Y).sum() / W.sum() - ((W*Y).sum()/W.sum())**2
                self._impurity_[node] = imp
                self[rnode]['impurity_is_updated'] = True
            return imp
        else:
            for node in self.allNodeConnections:
                self._impurity_[node] = self.impurity(node)
                self[node]['impurity_is_updated'] = True
            return self._impurity_

    def children_left(self,node):
        # print "Method: MyNestedTree.children_left()"
        rnode = self.real_node_id[tuple(node)]
        return self.allNodeConnections[rnode]['left child']

    def children_right(self,node):
        # print "Method: MyNestedTree.children_right()"
        rnode = self.real_node_id[tuple(node)]
        return self.allNodeConnections[rnode]['right child']

    def feature(self,node):
        # print "Method: MyNestedTree.feature()"
        return self._feature_[self.real_node_id[tuple(node)]]

    def threshold(self,node):
        # print "Method: MyNestedTree.threshold()"
        return self._threshold_[self.real_node_id[tuple(node)]]

    def n_node_samples(self,node):
        # print "Method: MyNestedTree.n_node_samples()"
        rnode = self.real_node_id[tuple(node)]
        return len(self.allData[rnode])

    def weighted_n_node_samples(self,node):
        # print "Method: MyNestedTree.weighted_n_node_samples()"
        rnode = self.real_node_id[tuple(node)]
        return self.allData[rnode].sample_weight.sum()

    def __getitem__(self,node_id):
        """This method is used internally, so do NOT change it on a whim!!"""
        # print "Method: MyNestedTree.__get__()"
        rnode = self.real_node_id[tuple(node_id)]
        return self.allNodeConnections[rnode]

    def get_sample_inds(self,node_id):
        # print "Method: MyNestedTree.get_sample_inds()"
        rnode = self.real_node_id[tuple(node_id)]
        return self.allData[rnode].sample_inds

    def get_node_data(self,node):
        # print "Method: MyNestedTree.get_node_data()"
        rnode = self.real_node_id[tuple(node)]
        return self.allData[rnode]

    def identify_nodes(self,old_node,new_node):
        # print "Method: MyNestedTree.identify_nodes()"
        self.real_node_id[tuple(old_node)] = new_node

    def add_subtree_nodes(self,T,new_tree_ind):
        # print "Method: MyNestedTree.add_subtree_nodes()"
        t_ind = new_tree_ind
        t = T['regressor'].tree_
        remaining_nodes = ones(t.children_left.shape).astype('bool')
        if remaining_nodes.shape[0] == 1:
            return
        leaf = (T['parent tree'],T['parent leaf'])
        self.real_node_id[(t_ind,0)] = (t_ind,0)
        if new_tree_ind > 0:
            self.real_node_id[leaf] = (t_ind,0)
            self.allNodeConnections[(t_ind,0)] = {'parent':self.allNodeConnections[leaf]['parent'], 
                                                  'left child':(t_ind,t.children_left[0] if t.children_left[0]>=0 else None), 
                                                  'right child':(t_ind,t.children_right[0] if t.children_right[0] >=0 else None),
                                                  'value_is_updated':False, 'impurity_is_updated':False}
            self.allNodeConnections[leaf] = dict(self.allNodeConnections[(t_ind,0)])
            p,s = self.allNodeConnections[(t_ind,0)]['parent']
            self.allNodeConnections[p][s] = (t_ind,0)
        else:
            self.allNodeConnections[(t_ind,0)] = {'parent':None, 
                                                  'left child':(t_ind,t.children_left[0] if t.children_left[0]>=0 else None), 
                                                  'right child':(t_ind,t.children_right[0] if t.children_right[0] >=0 else None),
                                                  'value_is_updated':False, 'impurity_is_updated':False}
        remaining_nodes[0] = False
        current_nodes = [0]
        while remaining_nodes.any():
            current_children = []
            for n in current_nodes:
                if t.children_left[n] >= 0:
                    l_ch, r_ch = t.children_left[n], t.children_right[n]
                    current_children += [l_ch,r_ch]
                    self.allNodeConnections[(t_ind,l_ch)] = {'parent':((t_ind,n),'left child'),
                                              'left child':(t_ind,t.children_left[l_ch]) if t.children_left[l_ch]>=0 else None,
                                              'right child':(t_ind,t.children_right[l_ch]) if t.children_right[l_ch] >=0 else None,
                                              'value_is_updated':False, 'impurity_is_updated':False}
                    self.allNodeConnections[(t_ind,r_ch)] = {'parent':((t_ind,n),'right child'),
                                              'left child':(t_ind,t.children_left[r_ch]) if t.children_left[r_ch]>=0 else None,
                                              'right child':(t_ind,t.children_right[r_ch]) if t.children_right[r_ch] >=0 else None,
                                              'value_is_updated':False, 'impurity_is_updated':False}
                    self.real_node_id[(t_ind,l_ch)] = (t_ind,l_ch)
                    self.real_node_id[(t_ind,r_ch)] = (t_ind,r_ch)
                    remaining_nodes[l_ch] = False
                    remaining_nodes[r_ch] = False
            current_nodes = list(current_children)

    def add_samples_to_tree(self,newTreeInd,Leaves,Data):
        """NOTE: This method should only be called after self.add_subtree_nodes() has been run!!"""
        # print "Method: MyNestedTree.add_sample_inds()"
        leaf_node_ids = [(newTreeInd,l) for l in Leaves]
        dNodeInds = tuples2dict(zip(leaf_node_ids,arange(len(Data))),Type='int')
        for node in dNodeInds:
            if node in self.allData:
                raise ValueError
            self.allData[node] = Data[dNodeInds[node]]
            ### DEBUG ###
            # print 'Created node ',node,'with sample size', len(self.allData[node])
            #############
        if len(leaf_node_ids) != len(Data):
            raise ValueError, 'ERROR: The lengths of leaf_node_ids and the Data should be the same'
        return leaf_node_ids

    def add_samples_to_leaf(self,leaf,Data):
        # print "Method: MyNestedTree.add_samples_to_leaf()"
        rleaf = self.real_node_id[tuple(leaf)]
        ### DEBUG ###
        # print 'Updating node '+str(leaf)+':', len(self.allData[rleaf]), '+', len(Data), '=', len(self.allData[rleaf])+len(Data), '>=?',
        #############
        self.allData[rleaf] |= Data
        ### DEBUG ###
        # print len(self.allData[rleaf])
        #############
        self[rleaf]['value_is_updated'] = False
        self[rleaf]['impurity_is_updated'] = False

    def get_leaves(self):
        # print "Method: MyNestedTree.get_leaves()"
        return [self.real_node_id[tuple(node_id)] 
                for node_id in self.allNodeConnections 
                if self.allNodeConnections[self.real_node_id[tuple(node_id)]]['left child'] is None]

    def get_leaves_below(self,parent):
        Leaves = []
        Nodes = [parent]
        NonLeaves = [node for node in Nodes if self[node]['left child'] is not None]
        Leaves += [node for node in Nodes if self[node]['left child'] is None]
        while len(NonLeaves) > 0:
            Nodes = [self[node][side] for node in NonLeaves for side in ['left child','right child']]
            NonLeaves = [node for node in Nodes if self[node]['left child'] is not None]
            Leaves += [node for node in Nodes if self[node]['left child'] is None]
        return Leaves

    def get_nodes(self):
        rnodes = sorted(set([self.real_node_id[node] for node in self.allNodeConnections]))
        return rnodes

    def update_samples_of_ancestors(self,input_leaves=None):
        # print "Method: MyNestedTree.update_samples_of_ancestors()"
        if input_leaves is None:
            leaves = self.get_leaves()
        elif type(input_leaves) == tuple:
            leaves = [input_leaves]
        else:
            leaves = [tuple(l) for l in input_leaves]
        parents_sides = [self.allNodeConnections[l]['parent'] for l in leaves if self.allNodeConnections[l]['parent'] is not None]
        while len(parents_sides) > 0:
            for parent, side in parents_sides:
                self[parent]['value_is_updated'] = False
                self[parent]['impurity_is_updated'] = False
                child = self.allNodeConnections[parent][side]
                parentData = self.allData.get(parent,None)
                self.allData[parent] = self.allData[child] | parentData
                ### DEBUG ###
                # print 'Updating node '+str(parent)+':', len(self.allData[parent]), '<=?' , len(self.allData[child]) + len(parentData) if parentData is not None else 0 , '=' , len(self.allData[child]), '+', len(parentData) if parentData is not None else 0
                #############
            parents_sides = [self.allNodeConnections[n]['parent'] for n in set(zip(*parents_sides)[0]) 
                                                                  if self.allNodeConnections[n]['parent'] is not None]

    def update_stats(self,T,tree_ind,parent_node_id=None):
        # NOTE: This method assumes that add_subtree_nodes() has been called with T as the input!
        # print "Method: MyNestedTree.update_stats()"
        for ind in range(T['regressor'].tree_.feature.shape[0]):
            self._children_left_[(tree_ind,ind)] = (tree_ind, T['regressor'].tree_.children_left[ind]) if T['regressor'].tree_.children_left[ind] > 0 else None
            self._children_right_[(tree_ind,ind)] = (tree_ind, T['regressor'].tree_.children_right[ind]) if T['regressor'].tree_.children_right[ind] > 0 else None
            self._feature_[(tree_ind,ind)] = T['regressor'].tree_.feature[ind]
            self._threshold_[(tree_ind,ind)] = T['regressor'].tree_.threshold[ind]
        if parent_node_id is not None:
            self._children_left_[parent_node_id] = self._children_left_[(tree_ind,0)]
            self._children_right_[parent_node_id] = self._children_right_[(tree_ind,0)]
            p = self.allNodeConnections[(tree_ind,0)]['parent'][0]
            self._children_left_[p] = self.allNodeConnections[p]['left child']
            self._children_right_[p] = self.allNodeConnections[p]['right child']
        self.value()
        self.impurity()

    def apply(self,X):
        Nodes = [(0,0) for _ in X]
        FTh = [(self.feature(node),self.threshold(node)) if self.feature(node) >= 0 
                                                         else None 
               for node in Nodes]
        while len([fth for fth in FTh if fth is not None]) > 0:
            Nodes = [node if f_th is None else (self.children_left(node) 
                                               if x[f_th[0]] <= f_th[1]
                                               else self.children_right(node))
                     for node,x,f_th in zip(Nodes,X,FTh)]
            FTh = [None if node is None else ((self.feature(node),self.threshold(node)) 
                                              if self.feature(node) >= 0 
                                              else None) 
                           for node in Nodes]
        return Nodes

    def add_split(self,node,feature,threshold,Data=None):
        '''Add a single split, rather than a whole subtree as in add_subtree_nodes()'''
        tree_number = min(zip(*self.allNodeConnections.keys())[0])-1
        f_th = {feature:threshold}
        # Add the parent node
        parent = (tree_number,0)
        self.real_node_id[node] = parent
        self.allNodeConnections[parent] = {'parent':self.allNodeConnections[node]['parent'],
                                            'left child':(tree_number,1), 
                                            'right child':(tree_number,2),
                                            'value_is_updated':False, 'impurity_is_updated':False}
        self._children_left_[parent] = (tree_number,1)
        self._children_right_[parent] = (tree_number,2)
        self._feature_[parent] = feature
        self._threshold_[parent] = threshold
        self.real_node_id[parent] = parent
        # Add the left child:
        left_child = (tree_number,1)
        self.allNodeConnections[left_child] = {'parent':(parent,'left child'),
                                                'left child':None, 
                                                'right child':None,
                                                'value_is_updated':False, 'impurity_is_updated':False}
        self._children_left_[left_child] = None
        self._children_right_[left_child] = None
        self._feature_[left_child] = None
        self._threshold_[left_child] = None
        self.real_node_id[left_child] = left_child
        # Add the right child
        right_child = (tree_number,2)
        self.allNodeConnections[right_child] = {'parent':(parent,'right child'),
                                                'left child':None, 
                                                'right child':None,
                                                'value_is_updated':False, 'impurity_is_updated':False}
        self._children_left_[right_child] = None
        self._children_right_[right_child] = None
        self._feature_[right_child] = None
        self._threshold_[right_child] = None
        self.real_node_id[right_child] = right_child
        # Split the data
        if Data is not None:
            vData = Data.copy_values() | self.allData[node]
        else:
            vData = self.allData[node]
        lData = vData[vData <= f_th]
        rData = vData[vData > f_th]
        self.allData[parent] = vData
        self.allData[(tree_number,1)] = lData
        self.allData[(tree_number,2)] = rData
        self.update_samples_of_ancestors(parent)
        return parent, left_child, right_child

    def update_values(self,nodes,values):
        for node, value in zip(nodes,values):
            rnode = self.real_node_id[tuple(node)]
            self._value_[rnode] = value
            self[rnode]['value_is_updated'] = True


if __name__ == "__main__":
    ''' TODO: Create unit test here!'''
    pass















"""  OLD METHODS ...

    def add_samples(self,X,Y,sample_weight):
        # print "Method: MyNestedTree.add_samples()"
        # Keep track of all samples used so far:
        if self.allX is None:
            start_ind = 0
            self.allX = X
            self.allY = Y
            self.allSampleWeight = sample_weight
        else:
            start_ind = self.allX.shape[0]
            self.allX = r_[self.allX,X]
            self.allY = r_[self.allY,Y]
            self.allSampleWeight = r_[self.allSampleWeight,sample_weight]
        end_ind = self.allY.shape[0]
        return start_ind, end_ind


    def get_samples(self,node_id):
        # print "Method: MyNestedTree.get_samples()"
        Inds = self.get_sample_inds(node_id)
        return self.allX[Inds,:],self.allY[Inds],self.allSampleWeight[Inds]

    def add_tree_nodes(self,parent_node_id,T):
        self.allNodeConnections[(newTreeInd,0)] = self.allNodeConnections[parent_node_id]
        parentNode, side = self.allNodeConnections[parent_node_id]['parent']
        self.allNodeConnections[parentNode][side] = (newTreeInd,0)

    def unionInds(sample_inds1,sample_Ys1,sample_inds2,sample_Ys2):
        samples1 = [tuple([ind])+tuple([y]) for ind,y zip(sample_inds1,sample_Ys1)]
        samples2 = [tuple([ind])+tuple([y]) for ind,y zip(sample_inds2,sample_Ys2)]
        samples = sorted(set(samples1+samples2))
        sample_inds,sample_Ys = zip(*samples)
        return array(sample_inds,dtype=self.sample_ind_dtype), array(sample_Ys)
"""
from numpy import *
set_printoptions(edgeitems=3, infstr='inf', linewidth=9999, nanstr='nan', precision=4, suppress=False, threshold=1000, formatter=None)
import matplotlib
matplotlib.use('Agg')
from matplotlib.pyplot import *
import MyNT
reload(MyNT)
from sklearn.tree import DecisionTreeRegressor
from sklearn.base import clone
from numpy.random import choice, rand, randint
from copy import deepcopy
import os
import cPickle as pkl
import gzip
import sys

# os.system('export LC_ALL=en_US.UTF-8')
# os.system('export LANG=en_US.UTF-8')

def list_sum(Ls):
    # Union of lists
    return [l for L in Ls for l in L]

def list_choice(L):
    ind = randint(len(L))
    return L[ind]

def choice2D(A,n_rows=None,n_cols=None):
    # A should be a 2 dimensional array
    sA = array(A)
    if len(sA.shape) != 2:
        raise ValueError("ERROR inside NRT.choice2D(): The input array needs to be convertible to a 2 dimensional array!")
    if n_rows is None and n_cols is None:
        raise ValueError("ERROR inside NRT.choice2D(): The one of n_rows and n_cols needs to be a number!")
    if n_cols is not None and n_cols < sA.shape[1]:
        n_cols = int(n_cols)
        sA = array([choice(a,size=n_cols) for a in sA])
    if n_rows is not None and n_rows < sA.shape[0]:
        n_rows = int(n_rows)
        tA = array(sA.T)
        tA = array([choice(a,size=n_rows) for a in tA])
    sA = tA.T
    return sA

def myArgmin(A):
    Inds = nonzero(A==A.min())[0]
    return choice(Inds)

def myArgmax(A):
    Inds = nonzero(A==A.max())[0]
    return choice(Inds)

def plotTree(T,fname):
    from sklearn.externals.six import StringIO
    if not fname.endswith('.pdf'):
        fname += '.pdf'
    try:
        import pydot
    except ImportError:
        print "WARNING in NRT.plotTree: Not producing tree plot because pydot could not be imported!"
        return
    from sklearn.tree import export_graphviz
    dot_data = StringIO()
    export_graphviz(T, out_file=dot_data, filled=True, node_ids=True, proportion=False, rounded=True)
    graph = pydot.graph_from_dot_data(dot_data.getvalue())
    if type(graph) == list:
        graph[0].write_pdf(fname)
    else:
        graph.write_pdf(fname)

def plotNestedTree(T,fname):
    from sklearn.externals.six import StringIO
    import NestedExport as NE
    reload(NE)
    if not fname.endswith('.pdf'):
        fname += '.pdf'
    try:
        import pydot
    except ImportError:
        print "WARNING in NRT.plotNestedTree: Not producing tree plot because pydot could not be imported!"
        return
    from sklearn.tree import export_graphviz
    dot_data = StringIO()
    NE.nested_export_graphviz(T, out_file=dot_data, filled=True, node_ids=True, proportion=False, rounded=True)
    graph = pydot.graph_from_dot_data(dot_data.getvalue())
    if type(graph) == list:
        graph[0].write_pdf(fname)
    else:
        graph.write_pdf(fname)

def subsample_list(L,fraction):
    nSamples = len(L)
    Inds = range(nSamples)
    N = min(int(fraction*nSamples)+1,nSamples)
    chInds = choice(Inds,size=N,replace=False)
    chInds.sort()
    chL = [L[ind] for ind in chInds]
    return chL

def save_obj(obj,fname,compress=True):
    if compress:
        if not fname.endswith('.gzpkl'):
            fname += '.gzpkl'
        wh = gzip.open(fname,'wb')
    else:
        if not fname.endswith('.pkl'):
            fname += '.pkl'
        wh = open(fname,'wb')
    pkl.dump(obj,wh)
    wh.close()

def load_obj(fname,is_compressed=True):
    if is_compressed or fname.endswith('.gzpkl'):
        if not fname.endswith('.gzpkl'):
            fname += '.gzpkl'
        fh = gzip.open(fname,'rb')
    elif not is_compressed or fname.endswith('.pkl'):
        if not fname.endswith('.pkl'):
            fname += '.pkl'
        fh = open(fname,'rb')
    obj = pkl.load(fh)
    fh.close()
    return obj


class NestedDecisionTreeRegressor:
    """A nested decision tree regressor.
        # This class needs to have the following methods: get_potential_candidates(), temp_update_estimator(), delete_last_candidate(), 
        #                                                 full_update_estimator(), evaluate_for_comparison(), plot(), best_result()
        NOTE: This class does NOT deal with more than one output: i.e. n_outputs = 1!
        """
    def __init__(self,DataGeneratorClass,
        param_values=None,
        folders=None,
        estimator_name=''):
        # print "Method: NestedDecisionTreeRegressor.__init__()"
        default_param_values = {'criterion':"mse",
                            'splitter':"best",
                            'max_depth':None,
                            'min_samples_split':200,
                            'min_samples_leaf':100,
                            'min_weight_fraction_leaf':0.,
                            'max_features':None,
                            'random_state':None,
                            'max_leaf_nodes':1000,
                            'presort':False,
                            'n_classes':[1]}
        if param_values is None:
            param_values = {}
        for k in default_param_values:
            if k not in param_values:
                param_values[k] = default_param_values[k]

        self.DataGenerator = DataGeneratorClass(fname_prefix=estimator_name,
                                                data_folder=folders['data'])
        self.estimator_name = estimator_name
        self.folders=folders
        self.criterion=param_values['criterion']
        self.splitter=param_values['splitter']
        self.max_depth=param_values['max_depth']
        self.min_samples_split=param_values['min_samples_split']
        self.min_samples_leaf=param_values['min_samples_leaf']
        self.min_weight_fraction_leaf=param_values['min_weight_fraction_leaf']
        self.max_features=param_values['max_features']
        self.max_leaf_nodes=param_values['max_leaf_nodes']
        self.random_state=param_values['random_state']
        self.presort=param_values['presort']
        self.n_classes=param_values['n_classes']
        self.n_outputs=1
        self.Trees = []
        self.reset_main_regressor()
        self.nestedTree = MyNT.MyNestedTree(criterion=self.criterion,
                                            n_outputs=self.n_outputs,
                                            n_classes=self.n_classes,
                                            sample_ind_dtype='i,i')

    def get_data(self):
        # Write new data to file
        self.DataGenerator.write_more_data_to_file()

    def reset_main_regressor(self):
        # print "Method: NestedDecisionTreeRegressor.reset_main_regressor()"
        self.main_regressor = DecisionTreeRegressor(
            criterion=self.criterion,
            splitter=self.splitter,
            max_depth=self.max_depth,
            min_samples_split=self.min_samples_split,
            min_samples_leaf=self.min_samples_leaf,
            min_weight_fraction_leaf=self.min_weight_fraction_leaf,
            max_features=self.max_features,
            max_leaf_nodes=self.max_leaf_nodes,
            random_state=self.random_state,
            presort=self.presort)

    def get_potential_candidates(self,max_num_candidates,data_subsample_rate=.1):
        """NOTE: This method does not return "empty" trees, i.e. trees with a single node!"""
        # print "Method: NestedDecisionTreeRegressor.get_potential_candidates()"

        # Get the last batch of data
        Data = self.DataGenerator.get_last_data_batch()

        # If there are no trees to extend, then get different trees by sub-sampling the data:
        if len(self.Trees) == 0:
            potTrees = []
            for _ in range(max_num_candidates):
                trData = Data.subsample(data_subsample_rate)
                T = {}
                self.reset_main_regressor()
                self.main_regressor.set_params(min_samples_leaf=int(self.min_samples_leaf*data_subsample_rate)+1,
                                               min_samples_split=int(self.min_samples_split*data_subsample_rate)+1)
                T['regressor'] = clone(self.main_regressor)
                T['regressor'].fit(trData.X,trData.Y,trData.sample_weight)
                if T['regressor'].tree_.feature.shape[0] == 1:
                    continue
                T['parent tree'] = None
                T['parent leaf'] = None
                T['fully added'] = False
                T['subtrees'] = {}
                # Add the tree to the potential trees
                potTrees.append(T)
        else:
            sData = Data.subsample(data_subsample_rate/2)
            # Find out which Leaves the various data point fall in
            Leaves = self.apply(sData.X)
            potTrees = []
            for _ in range(max_num_candidates):
                tempData = Data.subsample(data_subsample_rate/2)
                # Find out which Leaves the various data point fall in
                new_leaves = r_[Leaves, self.apply(tempData.X)]
                newData = sData+tempData

                # Choose a random data point from sData and see which leaf it falls in
                leaf = choice(Leaves)
                # Get the data points that fall inside the same leaf
                Inds = (new_leaves == leaf)
                newData = newData[Inds]
                # Also subsample the old data that lies inside our leaf already
                sample_inds = self.nestedTree.get_sample_inds(leaf)
                sample_inds = choice(sample_inds,int(sample_inds.shape[0]*data_subsample_rate)+1)
                oldData = self.DataGenerator[sample_inds]
                # Combine the old data with the new data to form the training data and train a tree on it
                try:
                    trData = oldData+newData
                except:
                    raise ValueError
                T = {}
                self.reset_main_regressor()
                self.main_regressor.set_params(min_samples_leaf=int(self.min_samples_leaf*data_subsample_rate)+1,
                                               min_samples_split=int(self.min_samples_split*data_subsample_rate)+1)
                T['regressor'] = clone(self.main_regressor)
                T['regressor'].fit(trData.X,trData.Y,trData.sample_weight)
                if T['regressor'].tree_.feature.shape[0] == 1:
                    continue
                T['parent tree'] = leaf[0]
                T['parent leaf'] = leaf[1]
                T['fully added'] = False
                T['subtrees'] = {}
                # Add the tree to the potential trees
                potTrees.append(T)
        return potTrees

    def best_result(self,Splits):
        # print "Method: NestedDecisionTreeRegressor.best_result()"
        goodSplits = [T for T in Splits if T['regressor'].tree_.value.shape[0] > 1]
        Rs = array([T['eval_result'] for T in goodSplits])
        best_ind = myArgmin(Rs)
        return goodSplits[best_ind]

    def temp_update_estimator(self,T):
        # print "Method: NestedDecisionTreeRegressor.temp_update_estimator()"
        Parent_Tree_ind = T['parent tree']
        Parent_Leaf_ind = T['parent leaf']
        self.Trees.append(T)
        if Parent_Tree_ind is not None:
            self.Trees[Parent_Tree_ind]['subtrees'][Parent_Leaf_ind] = len(self.Trees)-1

    def evaluate_for_comparison(self):
        # print "Method: NestedDecisionTreeRegressor.evaluate_for_comparison()"
        Data = self.DataGenerator.get_last_data_batch()
        result = self.getMSE(Data.X,Data.Y,Data.sample_weight)
        return result

    def delete_last_candidate(self):
        # print "Method: NestedDecisionTreeRegressor.delete_last_candidate()"
        pti = self.Trees[-1]['parent tree']
        pl = self.Trees[-1]['parent leaf']
        if pti is not None:
            del self.Trees[pti]['subtrees'][pl]
        return self.Trees.pop(-1)

    def full_update_estimator(self,T=None,final_update=True):
        # print "Method: NestedDecisionTreeRegressor.full_update_estimator()"
        Data = self.DataGenerator.get_last_data_batch()
        if len(self.Trees) == 0:
            # If this is the first tree being added ...
            if T is None:
                raise ValueError, 'ERROR inside NRT.NestedDecisionTreeRegressor.full_update_estimator(): The first tree should not be empty!'
            T['fully added'] = True
            self.temp_update_estimator(T)
            tLeaves = T['regressor'].apply(Data.X)
            self.nestedTree.add_subtree_nodes(T,0)
            leaf_node_ids = self.nestedTree.add_samples_to_tree(0,tLeaves,Data.copy_values())
            self.nestedTree.update_samples_of_ancestors()
            self.nestedTree.update_stats(T,0,None)
        elif T is None:
            Leaves = self.apply(Data.X)
            for leaf in unique(Leaves):
                # For all Leaves of the existing tree, add the new samples to the old samples in the leaf
                Inds = (Leaves == leaf)
                self.nestedTree.add_samples_to_leaf(leaf,Data.copy_values()[Inds])
        else:
            T['fully added'] = True
            Leaves = self.apply(Data.X)
            for leaf in unique(Leaves):
                # For all Leaves of the existing tree, add the new samples to the old samples in the leaf
                Inds = (Leaves == leaf)
                self.nestedTree.add_samples_to_leaf(leaf,Data.copy_values()[Inds])
            # For the parent node of T, also push the data down to the Leaves of the new tree
            newTreeInd = len(self.Trees)
            self.temp_update_estimator(T)
            parentNode = (T['parent tree'],T['parent leaf'])
            parentInds = self.nestedTree.get_sample_inds(parentNode)
            parentData = self.DataGenerator[parentInds]
            tLeaves = T['regressor'].apply(parentData.X)
            self.nestedTree.add_subtree_nodes(T,newTreeInd)
            leaf_node_ids = self.nestedTree.add_samples_to_tree(newTreeInd,tLeaves,parentData.copy_values())
            self.nestedTree.update_samples_of_ancestors()
            self.nestedTree.update_stats(T,newTreeInd)

    def apply(self,X):
        # print "Method: NestedDecisionTreeRegressor.apply()"
        if array([T['fully added'] for T in self.Trees]).all():
            return array(self.nestedTree.apply(X),dtype='i,i')
        else:
            Leaves = self.Trees[0]['regressor'].apply(X)
            TI_L = [(0,leaf) for leaf in Leaves]
            Subtrees = array([self.Trees[0]['subtrees'].get(leaf,-1) for leaf in Leaves])
            while (Subtrees >= 0).any():
                TI_L = [(tree_ind, self.Trees[tree_ind]['regressor'].apply(mat(X[ind,:]).A)[0]) if tree_ind >= 0
                                                                                                else TI_L[ind]
                                                                        for ind, tree_ind in enumerate(Subtrees)]
                Subtrees = array([self.Trees[tree_ind]['subtrees'].get(leaf,-1) for tree_ind, leaf in TI_L])
            return array(TI_L,dtype='i,i')

    def predict(self,X):
        # print "Method: NestedDecisionTreeRegressor.predict()"
        TI_L = self.apply(X)
        try:
            V = array([self.nestedTree.value((tree_ind,leaf)) 
                    if self.Trees[tree_ind]['fully added']
                    else self.Trees[tree_ind]['regressor'].tree_.value[leaf,0,0] 
                        for tree_ind, leaf in TI_L])
        except KeyError:
            raise ValueError
        return V

    def mean_std(self,X):
        # print "Method: NestedDecisionTreeRegressor.mean_std()"
        TI_L = self.apply(X)
        M_Std = [(self.Trees[tree_ind]['regressor'].tree_.value[leaf,0,0],self.Trees[tree_ind]['regressor'].tree_.impurity[leaf]) for tree_ind, leaf in TI_L]
        return M_Std

    def getMSE(self,vX,vY,vSW=None):
        # print "Method: NestedDecisionTreeRegressor.getMSE()"
        Y = self.predict(vX)
        if vSW is None:
            SE = (Y-vY)**2
        else:
            SE = (Y-vY)**2 * vSW
        return SE.mean()

    def plot(self,fname):
        # print "Method: NestedDecisionTreeRegressor.plot()"
        plotNestedTree(self.nestedTree,fname)

    def save(self,fname,compress=True):
        raise NotImplementedError

    @classmethod
    def load(cls,fname,is_compressed=True):
        raise NotImplementedError


class NestedLambdaMART:
    """A nested decision tree regressor.
        # This class needs to have the following methods: get_potential_candidates(), temp_update_estimator(), delete_last_candidate(), 
        #                                                 full_update_estimator(), evaluate_for_comparison(), plot(), best_result()
        NOTE: This class does NOT deal with more than one output: i.e. n_outputs = 1!
        """
    def __init__(self,DataGeneratorClass,
        param_values=None,
        folders=None,
        estimator_name='NestedLambdaMART'):
        from rankpy.models import LambdaMART
        from rankpy.gridsearch import gridsearch
        print "Method: NestedLambdaMART.__init__()"
        default_param_values = {'data_dir':                 os.environ['HOME']+'/online-learning-data/YLR2/',
                                'n_procs':                  1,
                                'min_query_leaf':           40,
                                'ucb_quantile':             6./10,
                                'lcb_quantile':             5./10,
                            'lambdamart_gridsearch_params': {'metric':                  ['nDCG@10'],
                                                            'n_estimators':             [None],
                                                            'min_samples_split':        [200],
                                                            'min_samples_leaf':         [100],
                                                            'max_features':             [0.5, None],
                                                            'random_state':             [None],
                                                            'max_leaf_nodes':           [4,8,16,32,64],
                                                            'shrinkage':                [0.1, 0.5],
                                                            'estopping':                [50]
                                                             }
                                }
        if param_values is None:
            param_values = {}
        elif type(param_values) == list and array([ ((type(p)==tuple or type(p)==list) and len(p)==2) for p in param_values]).all():
            param_values = dict(param_values)
        for k in default_param_values:
            if k not in param_values:
                param_values[k] = default_param_values[k]
        for k in default_param_values['lambdamart_gridsearch_params']:
            if k not in param_values['lambdamart_gridsearch_params']:
                param_values['lambdamart_gridsearch_params'][k] = default_param_values['lambdamart_gridsearch_params'][k]
            try:
                param_values['lambdamart_gridsearch_params'][k] = list(param_values['lambdamart_gridsearch_params'][k])
            except TypeError:
                param_values['lambdamart_gridsearch_params'][k] = [param_values['lambdamart_gridsearch_params'][k]]

        self.DataGenerator = DataGeneratorClass(fname_prefix=estimator_name,
                                                data_dir=param_values['data_dir'])
        self.folders=folders
        self.criterion='NestedLambdaMART'
        self.data_dir=param_values['data_dir']
        self.n_procs=param_values['n_procs']
        self.min_query_leaf=param_values['min_query_leaf']
        self.lcb_quantile=param_values['lcb_quantile']
        self.ucb_quantile=param_values['ucb_quantile']
        self.lambdamart_gridsearch_params = param_values['lambdamart_gridsearch_params']
        min_samples_split,min_samples_leaf = self.lambdamart_gridsearch_params['min_samples_split'], self.lambdamart_gridsearch_params['min_samples_leaf']
        if len(min_samples_split) == len(min_samples_leaf):
            self.lambdamart_gridsearch_params[('min_samples_split','min_samples_leaf')] = zip(min_samples_split,min_samples_leaf)
        else:
            self.lambdamart_gridsearch_params[('min_samples_split','min_samples_leaf')] = [(mss,msl) for mss in min_samples_split for msl in min_samples_leaf]
        del self.lambdamart_gridsearch_params['min_samples_split']
        del self.lambdamart_gridsearch_params['min_samples_leaf']
        self.nestedTree = MyNT.MyNestedTree(criterion=self.criterion,
                                            sample_ind_dtype=int,
                                            Data=self.DataGenerator.qfData)
        # Get the global model
        Train, Estop, Valid, Test = self.DataGenerator.train_validation_data()
        print "Doing a parameter grid search on", len(Train), "queries ...",
        sys.stdout.flush()
        try:
            Model,_ = gridsearch(LambdaMART, self.lambdamart_gridsearch_params, 
                                      training_queries=Train,
                                      estopping_queries=Estop,
                                      validation_queries=Valid,
                                      return_models=False, n_jobs=self.n_procs)
        except ValueError:
            raise ValueError
        print "Done!"
        num_queries = len(self.DataGenerator.validation_data())
        # print "Evaluating a model on", num_queries, "queries ...",
        # sys.stdout.flush()
        valid_performance = Model.evaluate(Test)
        # print "Done!"
        self.allModels = {self.nestedTree.get_leaves()[0]:{'ranker':Model,
                                                           'validation_performance':valid_performance,
                                                           'all_validation_performances':array([valid_performance]),
                                                           'validation_size':self.DataGenerator.training_validation_query_count()}}

    def get_data(self):
        # Write new data to file
        pass

    def get_potential_candidates(self,max_num_candidates,data_subsample_rate=.1):
        """NOTE: This method does not return "empty" trees, i.e. trees with a single node!"""
        print "Method: NestedLambdaMART.get_potential_candidates()"
        from collections import Counter
        param_grid = dict(self.lambdamart_gridsearch_params)
        # param_grid['n_estimators'] = [2]
        # param_grid['max_leaf_nodes'] = sorted(set([mln for mln in param_grid['max_leaf_nodes'] if mln < 20]+[4]))
        param_grid[('min_samples_split','min_samples_leaf')] = [(data_subsample_rate*mss,data_subsample_rate*msl)
                                                                for mss, msl in param_grid[('min_samples_split','min_samples_leaf')]]
        potSplits = []
        # Make local variables for query feature values
        query_features = self.DataGenerator.qfData.query_features
        training_validation_values = self.DataGenerator.qfData.get_training_validation_values(expanded=True)
        test_values = self.DataGenerator.qfData.get_test_values(expanded=True)
        # and find the leaves for each of them
        training_validation_leaves = array(self.nestedTree.apply(training_validation_values),dtype='i,i')
        test_leaves = array(self.nestedTree.apply(test_values),dtype='i,i')
        # Subsample the leaves to choose leaves to split, giving higher probability to fatter leaves
        n_queries = training_validation_values.shape[0]
        chInds = choice(range(n_queries),int(n_queries*data_subsample_rate),replace=False)
        chInds.sort()
        chLeaves = Counter([tuple(l) for l in training_validation_leaves[chInds]])
        # For each sampled leaf propose splits
        for leaf in chLeaves:
            # First, get the part of the data that falls inside the leaf
            training_validation_inds = (training_validation_leaves == array(leaf,dtype='i,i'))
            test_inds = (test_leaves == array(leaf,dtype='i,i'))
            DG = self.DataGenerator[training_validation_inds,test_inds]
            print "leaf, query_counts:", (leaf,DG.query_counts())
            # Get the training values falling inside this leaf
            training_validation_values_in_leaf = DG.qfData.get_training_validation_values(expanded=False)
            # Sample a few feature values for each feature
            thresholds_per_feature = 20. * max_num_candidates * chLeaves[leaf] / (training_validation_values_in_leaf.shape[1] * sum(chLeaves.values()))
            nSamples = int(ceil(thresholds_per_feature/2))
            nF = training_validation_values_in_leaf.shape[1]
            # Sample some values from the feature values of the queries ...
            sampled_values1 = choice2D(training_validation_values_in_leaf,n_rows=nSamples)
            # and some randomly chosen within the range of the feature
            sampled_values2 = ( training_validation_values_in_leaf.min(axis=0) 
                              + rand(nSamples,nF) * ( training_validation_values_in_leaf.max(axis=0)
                                                    - training_validation_values_in_leaf.min(axis=0) ) )
            # Append the max and min feature values
            sampled_values = vstack([training_validation_values_in_leaf.min(axis=0),
                                     sampled_values1,
                                     sampled_values2,
                                     training_validation_values_in_leaf.max(axis=0)])
            # Sort the values for each feature
            sampled_values.sort(axis=0)
            # Remove duplicate values
            sampled_values = [unique(sv) for sv in sampled_values.T]
            # Sample a threshold between the consecutive values of each feature 
            sampled_thresholds = [sv[:-1]+rand(sv.shape[0]-1)*diff(sv) for sv in sampled_values]
            # For each feature-threshold pair, split the data and train two LambdaMART models
            if self.n_procs > 1:
                from joblib import Parallel, delayed
            if self.n_procs == 1:
                # Note: the special case where the jobs are not parallelized is done separately for debugging and readability purposes
                Results = []
                for ind, thresholds in enumerate(sampled_thresholds):
                    f = query_features[ind]
                    for th in thresholds:
                        result = split_and_evaluate(leaf,f,th,query_features,DG,data_subsample_rate,param_grid,self.min_query_leaf,
                                                    self.lcb_quantile,self.ucb_quantile,n_queries)
                        Results.append(result)
            else:
                f_th = list_sum([[(query_features[f],th) for th in thresholds] for f, thresholds in enumerate(sampled_thresholds)])
                Results = Parallel(n_jobs=self.n_procs,
                                   backend='multiprocessing')(
                                delayed(split_and_evaluate,
                                        check_pickle=True)(leaf,f,th,query_features,DG,data_subsample_rate,param_grid,self.min_query_leaf,
                                                           self.lcb_quantile,self.ucb_quantile,n_queries)
                            for f, th in f_th)
            Results.sort(key=lambda t:t[0])
            potSplits += [leaf_f_th for performance_improvement, leaf_f_th in Results[-max_num_candidates:] if performance_improvement > 0]
        return potSplits

    def temp_update_estimator(self,T):
        print "Method: NestedLambdaMART.temp_update_estimator()"
        raise NotImplementedError

    def delete_last_candidate(self):
        print "Method: NestedLambdaMART.delete_last_candidate()"
        raise NotImplementedError

    def full_update_estimator(self,Split=None,final_update=True):
        print "Method: NestedLambdaMART.full_update_estimator()"
        # Split the leaf and its data gets distributed between the children inside MyNT.add_split()
        parent, left_child,right_child = self.nestedTree.add_split(Split['leaf'],Split['feature'],Split['threshold'])
        Inds = self.nestedTree.get_node_data(Split['leaf']).get_indices()
        DG = self.DataGenerator[Inds]
        if not final_update:
            self.intermediate_add_split(parent,left_child,right_child,DG,Split)
        if final_update:
            self.final_add_split(parent,left_child,right_child,DG,Split)

    def intermediate_add_split(self,parent,left_child,right_child,DG,Split):
        print "Method: NestedLambdaMART.intermediate_add_split()"
        leDG, gtDG = DG.split(feature=Split['feature'],threshold=Split['threshold'])
        Performances = cv_gridsearch(self.lambdamart_gridsearch_params, DG, nIter=5, n_jobs=1, chatty=True)
        lePerformances = cv_gridsearch(self.lambdamart_gridsearch_params, leDG, nIter=5, n_jobs=1, chatty=True)
        gtPerformances = cv_gridsearch(self.lambdamart_gridsearch_params, gtDG, nIter=5, n_jobs=1, chatty=True)
        N = DG.training_validation_query_count()
        leN = leDG.training_validation_query_count()
        gtN = gtDG.training_validation_query_count()
        self.allModels[parent] = dict(self.allModels[Split['leaf']])
        self.allModels[parent]['validation_performance'] = r_[self.allModels[parent]['all_validation_performances'],Performances]
        self.allModels[left_child] = {'ranker':None,
                                      'validation_performance':lePerformances,
                                      'all_validation_performances':lePerformances,
                                      'validation_size':leN}
        self.allModels[right_child] = {'ranker':None,
                                       'validation_performance':gtPerformances,
                                       'all_validation_performances':gtPerformances,
                                       'validation_size':gtN}

    def evaluate_for_comparison(self):
        print "Method: NestedLambdaMART.evaluate_for_comparison()"
        return dict(self.allModels)

    def update_all_valid_performances(self,Splits):
        print "Method: NestedLambdaMART.update_all_valid_performances()"
        # First of all, gather all the performances of the leaves that are being considered for splitting
        for Split in Splits:
            allModels = Split['eval_result']
            # Find the split being considered, i.e. the parent and its children
            tbdNodes = sorted([node for node in allModels if type(allModels[node]['validation_performance']) != float 
                                                          and allModels[node]['validation_size'] is not None])
            split_leaf = Split['leaf']
            parent = tbdNodes[0]
            left_child = tbdNodes[1]
            right_child = tbdNodes[2]
            if len(tbdNodes) != 3 or (parent[1],left_child[1],right_child[1]) != (0,1,2) or len(set([parent[0],left_child[0],right_child[0]])) > 1:
                raise ValueError("ERROR inside NestedLambdaMART.update_all_valid_performances(): "+
                    "There should be only three nodes with lists of performances, i.e. the parent of the last split and its children!")
            # Get the performances that have been calculated for the leaf ...
            Performances = allModels[parent]['validation_performance']
            # and add them to the list of performances in self.allModels
            self.allModels[split_leaf]['all_validation_performances'] = r_[self.allModels[split_leaf]['all_validation_performances'],Performances]

    def best_result(self,Splits):
        print "Method: NestedLambdaMART.best_result()"
        # First of all, gather all the performances of the leaves that are being considered for splitting
        self.update_all_valid_performances(Splits)
        # Secondly, look at the performance improvement of each proposed split
        for ind, Split in enumerate(Splits):
            allModels = Split['eval_result']
            # Find the split being considered, i.e. the parent and its children
            tbdNodes = sorted([node for node in allModels if type(allModels[node]['validation_performance']) != float 
                                                      and allModels[node]['validation_size'] is not None])
            split_leaf = Split['leaf']
            parent = tbdNodes[0]
            left_child = tbdNodes[1]
            right_child = tbdNodes[2]
            # Get the performances of split leaves
            lePerformances = allModels[left_child]['validation_performance']
            gtPerformances = allModels[right_child]['validation_performance']
            # Get query counts
            N = allModels[parent]['validation_size']
            leN = allModels[left_child]['validation_size']
            gtN = allModels[right_child]['validation_size']
            totN = self.DataGenerator.training_validation_query_count()
            # Calculate the performances of the split node
            lrPerformances = sorted([(leP * leN + gtP * gtN) / N for leP in lePerformances for gtP in gtPerformances])
            # Get performances of the unsplit leaf
            Performances = self.allModels[split_leaf]['all_validation_performances']
            # Get the LCB of the performance of the split node
            # lcbInd = int(round(.5 * len(lrPerformances)))-1
            lrMedian = median(lrPerformances)
            # Get the UCB of the performance of the unsplit node
            # ucbInd = int(round(self.ucb_quantile * len(Performances)))-1
            totMedian = median(Performances)
            Split['performance_improvement'] = (lrMedian-totMedian) * N/totN
        bestSplit = sorted(Splits, key=lambda S: S['performance_improvement'])[-1]
        if bestSplit['performance_improvement'] > 0:
            bestSplit['allModels'] = dict(self.allModels)
            return bestSplit
        else:
            return None

    def final_add_split(self,parent,left_child,right_child,DG,Split):
        print "Method: NestedLambdaMART.final_add_split()"
        from rankpy.models import LambdaMART
        from rankpy.gridsearch import gridsearch
        leDG, gtDG = DG.split(feature=Split['feature'],threshold=Split['threshold'])
        leTrain, leEstop, leValid, leTest = leDG.train_validation_data()
        gtTrain, gtEstop, gtValid, gtTest = gtDG.train_validation_data()
        print "Doing a parameter grid search on", len(leTrain), "queries ...",
        sys.stdout.flush()
        leModel,_ = gridsearch(LambdaMART, self.lambdamart_gridsearch_params, 
                                      training_queries=leTrain,
                                      estopping_queries=leEstop,
                                      validation_queries=leValid,
                                      return_models=False, n_jobs=self.n_procs)
        print "Done!"
        print "Doing a parameter grid search on", len(gtTrain), "queries ...",
        sys.stdout.flush()
        gtModel,_ = gridsearch(LambdaMART, self.lambdamart_gridsearch_params, 
                                      training_queries=gtTrain,
                                      estopping_queries=gtEstop,
                                      validation_queries=gtValid,
                                      return_models=False, n_jobs=self.n_procs)
        print "Done!"
        self.allModels[parent] = dict(self.allModels[Split['leaf']])
        self.allModels[left_child] = {'ranker':leModel,
                                      'validation_performance':leModel.evaluate(leTest),
                                      'all_validation_performances':Split['eval_result'][left_child]['all_validation_performances'],
                                      'validation_size':leDG.training_validation_query_count()}
        self.allModels[right_child] = {'ranker':gtModel,
                                       'validation_performance':gtModel.evaluate(gtTest),
                                       'all_validation_performances':Split['eval_result'][right_child]['all_validation_performances'],
                                       'validation_size':gtDG.training_validation_query_count()}
        for node in Split['allModels']:
            # Do a sanity check
            if self.allModels[node]['all_validation_performances'].shape[0] > Split['allModels'][node]['all_validation_performances'].shape[0]:
                raise ValueError("ERROR inside NestedLambdaMART.final_add_split(): The number of validation performances in the new split should be "+
                                 "at least as high as the number of validation performances in the old tree!\nNumber of old performances, number of new performances:"+
                                 str((self.allModels[node]['all_validation_performances'].shape[0], Split['allModels'][node]['all_validation_performances'].shape[0])) )
            self.allModels[node]['all_validation_performances'] = array(Split['allModels'][node]['all_validation_performances'])

    def apply(self,queries):
        '''queries is a list of Queries objects, each consisting of a single query and its documents'''
        print "Method: NestedLambdaMART.apply()"
        X = array([q.feature_vectors[0] for q in queries])
        Leaves = self.nestedTree.apply(X)
        return Leaves

    def predict(self,queries):
        print "Method: NestedLambdaMART.predict()"
        Leaves = self.apply(queries)
        Scores = [self.allModels[l]['ranker'].predict(q) for l,q in zip(Leaves,queries)]
        return Scores

    def evaluate_all(self,valid_or_test='validation'):
        print "Method: NestedLambdaMART.evaluate_all()"
        Leaves = self.nestedTree.get_leaves()
        # Get the leaves that have not been evaluated on the validation or test data, i.e. TBD leaves
        tbdLeaves = [leaf for leaf in Leaves if valid_or_test+'_performance' not in self.allModels[leaf] 
                                             or type(self.allModels[leaf][valid_or_test+'_performance']) != float]
        if len(tbdLeaves) > 0:
            # Get the qfData from the data stored in nestedTree
            tbdDataInds = [self.nestedTree.get_node_data(leaf).get_indices() for leaf in tbdLeaves]
            # Collect the validation or test data for each TBD leaf
            if valid_or_test == 'validation':
                tbdTestQ = [self.DataGenerator[dataInds].validation_data() for dataInds in tbdDataInds]
            elif valid_or_test == 'test':
                tbdTestQ = [self.DataGenerator[dataInds].test_data() for dataInds in tbdDataInds]
            else:
                raise ValueError("ERROR inside NestedLambdaMART.evaluate_all(): You can evaluate on either the validation or the test set! I do not understand "+
                    str(valid_or_test))
            # and evaluate the model in each leaf on the corresponding portion of the validation or test data
            for ind, leaf in enumerate(tbdLeaves):
                testQ = tbdTestQ[ind]
                model = self.allModels[leaf]
                num_queries = len(testQ)
                # print "Evaluating a model on", num_queries, "queries from", valid_or_test, " data inside leaf", leaf, "...",
                # sys.stdout.flush()
                self.allModels[leaf][valid_or_test+'_performance'] = model['ranker'].evaluate(testQ)
                self.allModels[leaf][valid_or_test+'_size'] = self.nestedTree.get_node_data(leaf).query_counts()[0 if valid_or_test=='validation'
                                                                                                                   else 1]
                # print "Done!"
        data_sizes = array([self.nestedTree.get_node_data(leaf).query_counts()[0 if valid_or_test=='validation'
                                                                                 else 1] 
                            for leaf in Leaves])
        test_performances = array([self.allModels[leaf][valid_or_test+'_performance'] for leaf in Leaves])
        return (data_sizes * test_performances).sum() / data_sizes.sum()

    def test_results(self,batch=0):
        print "Method: NestedLambdaMART.test_results()"
        # Test the original model on the test data
        Valid = self.DataGenerator.validation_data()
        Test = self.DataGenerator.test_data()
        init_model = self.allModels[(0,0)]['ranker']
        num_queries = len(Valid)+len(Test)
        # print "Evaluating a model on", num_queries, "queries ...",
        # sys.stdout.flush()
        init_valid_result = init_model.evaluate(Valid)
        init_test_result = init_model.evaluate(Test)
        # print "Done!"
        # Test the final model on the test data
        final_valid_result = self.evaluate_all(valid_or_test='validation')
        final_test_result = self.evaluate_all(valid_or_test='test')
        print "init_valid_result,final_valid_result:", (init_valid_result,final_valid_result)
        print "init_test_result,final_test_result:", (init_test_result,final_test_result)
        return init_test_result, final_test_result

    def plot(self,fname):
        print "Method: NestedLambdaMART.plot()"
        plotNestedTree(self.nestedTree,fname)

    def save(self,fname,compress=True):
        print "Method: NestedLambdaMART.save()"
        NLM = deepcopy(self)
        NLM.DataGenerator = self.DataGenerator.copy_to_save()
        save_obj(NLM,fname,compress)

    @classmethod
    def load(cls,fname,is_compressed=True):
        print "Method: NestedLambdaMART.load()"
        NLM = load_obj(fname,is_compressed)
        NLM.DataGenerator.recover_from_saved()
        return NLM

def split_and_evaluate(leaf,feature,threshold,query_features,DG,in_data_subsample_rate,param_grid,min_query_leaf,lcb_quantile,ucb_quantile,totN):
    try:
        leDG, gtDG = DG.split(feature=feature,threshold=threshold)
    except ValueError:
        return (None, {'leaf':leaf,'feature':feature,'threshold':threshold})
    N = DG.training_validation_query_count()
    leN = leDG.training_validation_query_count()
    gtN = gtDG.training_validation_query_count()
    if N != (leN+gtN):
        raise ValueError("ERROR inside NRT.split_and_evaluate(): Something has gone horribly wrong somewhere! The number of queries in the left and right child leaves are "+
            str(leN)+" and "+str(gtN)+" which does not add up to the number of queries in their parent node, which is "+str(N))
    if min(leDG.query_counts()) > 0 and min(gtDG.query_counts()) > 0 and min(leN,gtN) > min_query_leaf:
        data_subsample_rate = max(in_data_subsample_rate,.5*min_query_leaf/N)
        leData_subsample_rate = max(in_data_subsample_rate,.5*min_query_leaf/leN)
        gtData_subsample_rate = max(in_data_subsample_rate,.5*min_query_leaf/gtN)
        # Do a quick parameter sweep to find the best performance in the left leaf
        lePerformances = get_performances(leDG,leData_subsample_rate,param_grid)
        # Do a quick parameter sweep to find the best performance in the right leaf
        gtPerformances = get_performances(gtDG,gtData_subsample_rate,param_grid)
        # Do the same parameter sweep as for the leaves on the unsplit node to see if the split has a positive or a negative effect
        Performances = get_performances(DG,data_subsample_rate,param_grid)
        # Calculate the performance obtained from splitting the leaf and training two separate rankers
        lrPerformances = sorted([(leP * leN + gtP * gtN) / N for leP in lePerformances for gtP in gtPerformances])
        # Get the LCB of the performance of the split node
        lcbInd = int(floor(lcb_quantile * len(lrPerformances)))-1
        lrLCB = lrPerformances[lcbInd]
        # Get the UCB of the performance of the unsplit node
        ucbInd = int(ceil(ucb_quantile * len(Performances)))-1
        totUCB = Performances[ucbInd]
        if lrLCB-totUCB > 0:
            print ("NRT.split_and_evaluate(f="+str(feature)+",th="+str(threshold)+"):"
                   +" Did a parameter sweep on a split with left and right child train/valid/test query counts "
                   +str(leDG.query_counts())+" and "+str(gtDG.query_counts())
                   +"\nMedianUnsplit_performance-MedianSplit_performance "
                   +str(median(lrPerformances))+"-"+str(median(Performances))+"="+str(median(lrPerformances)-median(Performances))
                   +"\nMeanUnsplit_performance-MeanSplit_performance "
                   +str(mean(lrPerformances))+"-"+str(mean(Performances))+"="+str(mean(lrPerformances)-mean(Performances)))
        # Return the difference between the performance of this specialized ranker vs the performance of the un-specialized ranker
        return ((lrLCB-totUCB)*N/totN,{'leaf':leaf,'feature':feature,'threshold':threshold})
    else:
        return (None, {'leaf':leaf,'feature':feature,'threshold':threshold})

def get_performances(DG,data_subsample_rate,param_grid,lower_quantile=4./10,upper_quantile=7./10,nIter=10):
    Best_performances = cv_gridsearch(param_grid, DG, n_jobs=1,
                            train_portion=.7*data_subsample_rate,estop_portion=.1,
                            valid_portion=.1*data_subsample_rate,
                            test_portion=.1*data_subsample_rate,
                            nIter=nIter)
    return Best_performances #[int(round(lower_quantile*nIter))-1], Best_performances[int(round(upper_quantile*nIter))-1]

def cv_gridsearch(param_grid, DG, n_jobs=1,
    train_portion=.7,estop_portion=.1,valid_portion=.1,test_portion=.1,nIter=10,chatty=False):
    from rankpy.models import LambdaMART
    from rankpy.gridsearch import gridsearch
    from time import time
    from datetime import datetime
    Best_performances = zeros(nIter)
    for ind in range(nIter):
        if chatty:
            t = time()
            t = datetime.fromtimestamp(t).strftime('%Y-%m-%d %H:%M:%S')
            print t,"- NRT.cv_gridsearch(): iteration number", ind+1
        (training_queries,
         estopping_queries,
         validation_queries,
         test_queries) = DG.train_validation_data(train_portion=train_portion,
                                                   estop_portion=estop_portion,
                                                   valid_portion=valid_portion,
                                                   test_portion=test_portion)
        if min(len(training_queries),len(validation_queries),len(test_queries)) == 0:
            raise ValueError("ERROR inside NRT.cv_gridsearch(): The method calling this function should make sure that there is enough data for training and validation!"+
                             " len(training_queries),len(validation_queries) = "+str(len(training_queries),len(validation_queries))+
                             " train_portion,valid_portion,estop_portion = "+str(train_portion,valid_portion,estop_portion) )
        Model, _ = gridsearch(LambdaMART, param_grid, 
                                training_queries=training_queries,
                                estopping_queries=estopping_queries,
                                validation_queries=validation_queries,
                                return_models=False, n_jobs=n_jobs)
        Best_performances[ind] = Model.evaluate(test_queries)
    Best_performances.sort()
    return Best_performances






ParamValues = {'criterion':"mse",
    'splitter':"best",
    'max_depth':None,
    'min_samples_split':200,
    'min_samples_leaf':100,
    'min_weight_fraction_leaf':0.,
    'max_features':None,
    'random_state':None,
    'max_leaf_nodes':1000,
    'presort':False,
    'n_classes':[1],
    'n_regressors':1,
    'n_procs':1,
    'gamma':.5,
    'n_FQI_iterations':5,
    'sample_tree':False,
    'forest_subsample_rate':1}

if __name__ == "__main__":
    from numpy.random import randn, choice
    from collections import Counter
    from DG import iidDG
    X = randn(10000,2)
    Y = (X[:,0]>X[:,1])-.5
    NRT = NestedDecisionTreeRegressor(DataGeneratorClass=iidDG,param_values=ParamValues,folders={'data':'./DATA'})
    NRT.temp_update_estimator(X[:1000,:],Y[:1000],max_leaf_nodes=4)
    Leaves = NRT.Trees[0]['regressor'].apply(X)
    Inds, = nonzero(Leaves==choice(Leaves))
    NRT.temp_update_estimator(X[Inds,:],Y[Inds],max_leaf_nodes=5)
    plotTree(NRT.Trees[0]['regressor'],'testNRT.pdf')
    NRT.plot('testPlotNestedTree.pdf')






















from numpy import *
set_printoptions(edgeitems=3, infstr='inf', linewidth=9999, nanstr='nan', precision=4, suppress=False, threshold=1000, formatter=None)
import sys
from sklearn.base import clone
from numpy.random import choice, rand
import os, re
from time import time, sleep
import cPickle as pkl
import gzip
import NRT
reload(NRT)
import DG
reload(DG)
from copy import deepcopy

def save_obj(obj,fname,compress=True):
    if compress:
        if not fname.endswith('.gzpkl'):
            fname += '.gzpkl'
        wh = gzip.open(fname,'wb')
    else:
        if not fname.endswith('.pkl'):
            fname += '.pkl'
        wh = open(fname,'wb')
    pkl.dump(obj,wh)
    wh.close()

def load_obj(fname,is_compressed=True):
    if is_compressed or fname.endswith('.gzpkl'):
        if not fname.endswith('.gzpkl'):
            fname += '.gzpkl'
        fh = gzip.open(fname,'rb')
    elif not is_compressed or fname.endswith('.pkl'):
        if not fname.endswith('.pkl'):
            fname += '.pkl'
        fh = open(fname,'rb')
    obj = pkl.load(fh)
    fh.close()
    return obj

def candidateInd2fname(estimator_ind,candidate_ind,candidate_batch):
    # Convert candidate info into file name
    return '__estimator_'+str(estimator_ind)+'__candidate_'+str(candidate_ind)+'__batch_'+str(candidate_batch)

def fname2candidateInd(fname):
    # Extract candidate info from file name
    strInds = zip(*re.findall(r'__([a-zA-Z]+)_(\d+)',fname))[1]
    return [int(ind) for ind in strInds]

def findEstimatorBatchFiles(folder,estimator_ind,candidate_batch):
    # Find all files relating to a estimator and candidate batch
    return [folder+f for f in os.listdir(folder) if '__estimator_'+str(estimator_ind) in f and '__batch_'+str(candidate_batch) in f]

def estimatorInd2fname(estimator_ind):
    # Convert estimator index into file name
    return 'Estimator_'+str(estimator_ind)

def fname2estimatorInd(fname):
    # Extract estimator index from file name
    strInd = re.findall(r'Estimator_(\d+)',fname)[0]
    return int(strInd)


class SynchNestedForestGrower:
    """This is a warm-up version of NestedRandomForest where the computations all happen on a single machine
       so there is easy access to a common folder on disk. ALSO, this class does NOT deal with memory issues!"""
    def __init__(self,
        DataGeneratorClass=DG.iidDG,
        NestedEstimatorModelClass=NRT.NestedDecisionTreeRegressor,
        NTMParams=None,
        base_dir='.',
        job_name='TEST',
        n_estimators=1,
        data_subsample_rate=.1,
        max_num_candidates_per_batch=5,
        test_full_candidates=True,
        n_procs=1):
        print "Method: SynchNestedForestGrower.__init__()"
        # NestedEstimatorModelClass is assumed to have the following methods: get_potential_candidates(), temp_update_estimator(), delete_last_candidate(), 
        #                                                         full_update_estimator(), evaluate_for_comparison(), plot(), best_result()
        # DataGeneratorClass is assumed to have the following methods: get_more_data(), write_more_data_to_file()
        self.n_estimators = n_estimators
        self.data_subsample_rate = data_subsample_rate
        self.max_num_candidates_per_batch = max_num_candidates_per_batch
        self.n_procs = n_procs
        self.test_full_candidates = test_full_candidates
        self.candidate_batch = 0
        self.folders = {}
        self.folders['info'] = base_dir+'/INFO/'+job_name+'/'
        self.folders['estimators'] = base_dir+'/ESTIMATORS/'+job_name+'/'
        self.folders['data'] = base_dir+'/DATA/'+job_name+'/'
        self.folders['todo'] = base_dir+'/TODO/'+job_name+'/'
        self.folders['claimed'] = base_dir+'/CLAIMED/'+job_name+'/'
        self.folders['done'] = base_dir+'/DONE/'+job_name+'/'
        self.folders['old'] = base_dir+'/OLD/'+job_name+'/'
        self.folders['logs'] = base_dir+'/LOGS/'+job_name+'/'
        for D in self.folders.values():
            T0 = time()
            if not os.path.isdir(D):
                os.makedirs(D)
        self.NestedEstimatorModelClass = NestedEstimatorModelClass
        Estimators = [NestedEstimatorModelClass(DataGeneratorClass=DataGeneratorClass,
                                                param_values=NTMParams,
                                                folders=self.folders,
                                                estimator_name='Estimator'+str(ind)) 
                                    for ind in range(n_estimators)]
        for ind,T in enumerate(Estimators):
            self.save_estimator(T,ind)

    def log(self,JobID,S):
        ah = open(self.folders['logs']+'Batch_'+str(self.candidate_batch)+'__JobID_'+str(JobID)+'.log', 'a')
        ah.write(S+'\n')
        ah.close()

    def merge_logs(self):
        Fs = sorted([self.folders['logs']+f for f in os.listdir(self.folders['logs'])])
        wh = open(self.folders['logs']+'AllLogs.log','a')
        for f in Fs:
            fh = open(f)
            wh.write(fh.read())
            fh.close()
        wh.close()

    def save_estimator(self,T,t_ind):
        print "Method: SynchNestedForestGrower.save_estimator()"
        f = self.folders['estimators']+estimatorInd2fname(t_ind)
        try:
            T.save(f,COMPRESS_TEMP_FILES)
        except NotImplementedError:
            save_obj(T,f,COMPRESS_TEMP_FILES)

    def load_estimator(self,t_ind):
        print "Method: SynchNestedForestGrower.load_estimator()"
        f = self.folders['estimators']+estimatorInd2fname(t_ind)
        try:
            estimator = self.NestedEstimatorModelClass.load(fname=f,is_compressed=COMPRESS_TEMP_FILES)
        except NotImplementedError:
            estimator = load_obj(f,COMPRESS_TEMP_FILES)
        return estimator

    def get_estimator_files(self):
        # Find all files relating to a estimator and candidate batch
        return [self.folders['estimators']+f for f in os.listdir(self.folders['estimators']) if 'Estimator_' in f]

    def load_all_estimators(self):
        print "Method: SynchNestedForestGrower.load_all_estimators()"
        Estimators = [self.load_estimator(ind) for ind in range(self.n_estimators)]
        return Estimators

    def clean_folders(self):
        for D in self.folders.values():
            if D != self.folders['estimators'] and os.path.isdir(D):
                for f in os.listdir(D):
                    os.remove(D+f)

    def get_candidates(self):
        print "Method: SynchNestedForestGrower.get_candidates()"
        # Get potential candidates to be evaluated and compared against each other
        n_candidates = 0
        for estimator_ind, Estimator in enumerate(self.load_all_estimators()):
            Candidates = Estimator.get_potential_candidates(max_num_candidates=self.max_num_candidates_per_batch,
                                                            data_subsample_rate=self.data_subsample_rate)
            for candidate_ind, candidate in enumerate(Candidates):
                try:
                    save_obj(Candidates[candidate_ind],self.folders['todo']+candidateInd2fname(estimator_ind,candidate_ind,self.candidate_batch),COMPRESS_TEMP_FILES)
                    n_candidates += 1
                except:
                    # If there was a problem saving this candidate, just skip it!
                    pass
        return n_candidates

    def claim_candidates(self,n_candidates=1,job_num=None):
        print "Method: SynchNestedForestGrower.claim_candidates()"
        JobID = str(abs(hash(rand())))
        Fs = [self.folders['todo']+f for f in os.listdir(self.folders['todo']) if '__estimator_' in f and '__candidate_' in f]
        if len(Fs) == 0:
            return None
        nFs = len(Fs)
        for _ in range(min(int(n_candidates),nFs)):
            Fs = [self.folders['todo']+f for f in os.listdir(self.folders['todo']) if '__estimator_' in f and '__candidate_' in f]
            if job_num is None:
                # If no job number has been passed to this method, choose a random candidate to evaluate
                oldf = choice(Fs)
            else:
                oldf = Fs[0]
                JobID = job_num
            t_ind,s_ind,b_ind = fname2candidateInd(oldf)
            pkl_ext = '.'+oldf.split('.')[-1]
            newf = self.folders['claimed']+candidateInd2fname(t_ind,s_ind,b_ind)+'_JobID'+JobID+pkl_ext
            try:
                os.rename(oldf,newf)
            except:
                pass
        return JobID

    def evaluate_candidate(self,estimator_ind,candidate,JobID):
        print "Method: SynchNestedForestGrower.evaluate_candidate()"
        if self.test_full_candidates:
            try: Estimator = self.load_estimator(estimator_ind)
            except NotImplementedError:
                self.log(JobID,"Inside RFG.SynchNestedForestGrower.evaluate_candidate(): Could not load estimator"+str(estimator_ind))
                raise ValueError

            try: Estimator.full_update_estimator(candidate,final_update=False)
            except NotImplementedError:
                self.log(JobID,"Inside RFG.SynchNestedForestGrower.evaluate_candidate(): Could not update estimator"+str(estimator_ind)+"with candidate"+str(candidate))
                raise ValueError

            try: eval_result = Estimator.evaluate_for_comparison()
            except NotImplementedError:
                self.log(JobID,"Inside RFG.SynchNestedForestGrower.evaluate_candidate(): Could not evaluate estimator"+str(estimator_ind))
                raise ValueError
        else:
            try:
                Estimator = self.load_estimator(estimator_ind)
                Estimator.temp_update_estimator(candidate)
                eval_result = Estimator.evaluate_for_comparison()
                Estimator.delete_last_candidate()
            except NotImplementedError:
                self.test_full_candidates = True
                eval_result = self.evaluate_candidate(estimator_ind,candidate,JobID)
        return eval_result

    def claim_and_eval(self,n_candidates=1):
        print "Method: SynchNestedForestGrower.claim_and_eval()"
        JID = self.claim_candidates(n_candidates)
        evaluate_claimed_candidates(self,JID)

    def gather_results(self):
        print "Method: SynchNestedForestGrower.gather_results()"
        BestCandidates = {}
        for estimator_ind in range(self.n_estimators):
            # Get the file names of the results for each estimator and the current batch
            Fs = findEstimatorBatchFiles(self.folders['done'],estimator_ind,self.candidate_batch)
            # If there were no potential candidate for this estimator, ...
            if len(Fs) == 0:
                # then set the best candidate to be None.
                BestCandidates[estimator_ind] = None
                if self.candidate_batch == 0:
                    raise ValueError('ERROR in RFG.SynchNestedForestGrower.gather_results(): '+
                                        'The results in the first batch must not be empty!')
            else:
                # Otherwise, extract the evaluation results
                Candidates = [load_obj(f,COMPRESS_TEMP_FILES) for f in Fs]
                # and let the Nested Estimator object decide the index of the best candidate 
                # and finally record the best candidate for each estimator
                BestCandidates[estimator_ind] = self.load_estimator(estimator_ind).best_result(Candidates)
                # Remove the results
                newFs = [re.sub(self.folders['done'],self.folders['old'],f) for f in Fs]
                for oldf, newf in zip(Fs,newFs):
                    os.rename(oldf,newf)
        # return the best candidates for each estimator
        return BestCandidates

    def final_updates(self,BestCandidates):
        print "Method: SynchNestedForestGrower.final_updates()"
        for estimator_ind in range(self.n_estimators):
            if BestCandidates[estimator_ind] is not None:
                Estimator = self.load_estimator(estimator_ind)
                Estimator.full_update_estimator(BestCandidates[estimator_ind],final_update=True)
                self.save_estimator(Estimator,estimator_ind)
            else:
                print "RFG.SynchNestedForestGrower.final_update(): NOTE that there were no extensions for estimator number",estimator_ind,"that led to an improvement!"

    def grow_estimators(self):
        """Add candidates to the existing estimators by first getting lists of candidates to evaluate 
           and then choosing the best candidate for each estimator"""
        print "Method: SynchNestedForestGrower.grow_estimators()"
        # Put some data in the right place
        get_data(self)
        # Get potential candidates to be tested and compared
        n_candidates = self.get_candidates()
        print "SynchNestedForestGrower.grow_estimators():",n_candidates,"candidates returned;", self.max_num_candidates_per_batch*self.n_estimators,"were requested."
        # Divide the evaluation tasks among the processes
        divide_tasks(self,n_candidates)
        # Collect the results
        BestCandidates = self.gather_results()
        # Add the best candidates to each estimator
        self.final_updates(BestCandidates)
        # Increment the index of the batch of candidates being considered
        self.candidate_batch += 1

def get_estimator_data(SNFG,ind):
    print "Method: RFG.get_estimator_data()"
    Estimator = SNFG.load_estimator(ind)
    Estimator.get_data()

def get_data(SNFG):
    print "Method: RFG.get_data()"
    N = SNFG.n_estimators
    if SNFG.n_procs == 1 or N == 1:
        for ind in range(N):
            get_estimator_data(SNFG,ind)
    else:
        from multiprocessing import Pool
        pool = Pool(processes=SNFG.n_procs)
        for ind in range(N):
            pool.apply_async(get_estimator_data, (SNFG,ind))
        pool.close()
        pool.join()  

def evaluate_claimed_candidates(SNFG,JobID):
    """For each claimed candidate with the code JobID, add the candidate to the corresponding extension, evaluate the results
       and REMOVE the extension. In other words, at the end of each iteration of the for loop the extension is the same as min_weight_fraction_leaf
       it was at the beginning of the method."""
    print "Method: RFG.evaluate_claimed_candidates()"
    # sleep(.1 * rand())
    Fs = try_reading_files(SNFG,JobID)
    # Evaluate each potential candidate
    for f in Fs:
        oldf = f
        t_ind,s_ind,b_ind = fname2candidateInd(oldf)

        try:
            extension = load_obj(f,COMPRESS_TEMP_FILES)
        except NotImplementedError:
            SNFG.log(JobID,"Inside RFG.evaluate_claimed_candidates(): Could not load "+f+" ... moving on!")
            continue

        try:
            SNFG.log(JobID,"RFG.evaluate_claimed_candidates(): evaluating candidate in file"+f)
            extension['eval_result'] = SNFG.evaluate_candidate(t_ind,extension,JobID)
        except NotImplementedError:
            SNFG.log(JobID,"Inside RFG.evaluate_claimed_candidates(): Could not evaluate the candidate in "+f+" ... moving on!")
            continue

        newf = SNFG.folders['done']+candidateInd2fname(t_ind,s_ind,b_ind)+'_JobID'+JobID

        try:
            save_obj(extension,newf,COMPRESS_TEMP_FILES)
        except NotImplementedError:
            SNFG.log(JobID,"Inside RFG.evaluate_claimed_candidates(): Could not save "+newf+" ... moving on!")
            continue

        try:
            os.remove(oldf)
        except:
            SNFG.log(JobID,"Inside RFG.evaluate_claimed_candidates(): Could not delete "+oldf+" ... moving on!")
            continue

def divide_tasks(SNFG,n_candidates):
    print "Method: RFG.divide_tasks()"
    if SNFG.n_procs == 1:
        SNFG.claim_and_eval(n_candidates)
    else:
        JIDs = []
        SNFGs = []
        for ind in range(SNFG.n_procs):
            SNFGs.append(deepcopy(SNFG))
            n_candidates_per_proc = ceil(1.*n_candidates/SNFGs[ind].n_procs)
            JID = SNFGs[ind].claim_candidates(n_candidates_per_proc)
            JIDs += [JID] if JID is not None else []
        from multiprocessing import Pool
        pool = Pool(processes=SNFGs[ind].n_procs)
        for ind, JID in enumerate(JIDs):
            sleep(.1)
            pool.apply_async(evaluate_claimed_candidates, (SNFGs[ind],JID))
        pool.close()
        pool.join()  

def try_reading_files(SNFG,JobID):
    try: Fs = [SNFG.folders['claimed']+f for f in os.listdir(SNFG.folders['claimed']) if '_JobID'+JobID in f]
    except:
        SNFG.log(JobID,"Inside RFG.evaluate_claimed_candidates(): Could not get a list of files from"+SNFG.folders['claimed'])
        try: Fs = [SNFG.folders['claimed']+f for f in os.listdir(SNFG.folders['claimed']) if '_JobID'+JobID in f]
        except:
            raise IOError, "Inside RFG.evaluate_claimed_candidates(): Could not get a list of files from"+SNFG.folders['claimed']
    if len(Fs) == 0:
        raise ValueError("ERROR inside RFG.try_reading_files(): There are no files to read!")
    return Fs

IIDParamValues = {
    'criterion':                  "mse",
    'splitter':                     "best",
    'max_depth':                    None,
    'min_samples_split':            200,
    'min_samples_leaf':             100,
    'min_weight_fraction_leaf':     0.,
    'max_features':                 None,
    'random_state':                 None,
    'max_leaf_nodes':               1000,
    'presort':                      False,
    'n_classes':                    [1],
    'n_regressors':                 1,
    'n_procs':                      1
}

LTRParamValues = {
    'criterion':                  "mse",
    'splitter':                     "best",
    'max_depth':                    None,
    'min_samples_split':            200,
    'min_samples_leaf':             100,
    'min_weight_fraction_leaf':     0.,
    'max_features':                 None,
    'random_state':                 None,
    'max_leaf_nodes':               1000,
    'presort':                      False,
    'n_classes':                    [1],
    'n_regressors':                 1,
    'n_procs':                      10,
    'gamma':                        .5,
    'n_FQI_iterations':             5,
    'sample_tree':                  False,
    'forest_subsample_rate':        1,
    'mdp_params':                   {'name':'Pitfall-ram-v0','dim':10},
    'data_dir':                     os.environ['HOME']+'/online-learning-data/YLR1/',
    'min_query_leaf':               100,
    'ucb_quantile':                 7./10,
    'lcb_quantile':                 4./10,
    'lambdamart_gridsearch_params': {'metric':                  ['nDCG@10'],
                                    'n_estimators':             [100],
                                    'min_samples_split':        [200],
                                    'min_samples_leaf':         [100],
                                    'max_features':             [.1,.5], #, None],
                                    'random_state':             [None],
                                    'max_leaf_nodes':           [4,8,16,32,64],
                                    'shrinkage':                [.1,.5],
                                    'estopping':                [50]
                                    }
}


if __name__ == "__main__":
    COMPRESS_TEMP_FILES = False
    if os.path.isdir('/dev/shm'):
        BASE_DIR = '/dev/shm'
    else:
        BASE_DIR = '.'

    PROBLEM = 'LTR'
    if len(sys.argv) > 1:
        PROBLEM = sys.argv[1]
    JOB_NAME = 'TEST'+PROBLEM
    if PROBLEM == 'IID':
        DGClass = DG.iidDG
        NEMClass = NRT.NestedDecisionTreeRegressor
        ParamValues = IIDParamValues
    elif PROBLEM == 'LTR':
        DGClass = DG.ltrDG
        NEMClass = NRT.NestedLambdaMART
        ParamValues = LTRParamValues
    else:
        raise ValueError("ERROR inside RFG.__main__: The input argument needs to be ones of IID, FQI or LTR! Not"+sys.argv[1])

    FG = SynchNestedForestGrower(DataGeneratorClass=DGClass,
        NestedEstimatorModelClass=NEMClass,
        NTMParams=ParamValues,
        base_dir=BASE_DIR,
        job_name=JOB_NAME,
        n_estimators=1,
        data_subsample_rate=.1,
        max_num_candidates_per_batch=1,
        test_full_candidates=True,
        n_procs=1)
    FG.clean_folders()
    for update_num in range(1,21):
        print 80*'='
        print 'Update',update_num,'...'
        print 80*'='
        FG.grow_estimators()
        if not os.path.isdir('FIGS'):
            os.makedirs('FIGS')
        for ind, Estimator in enumerate(FG.load_all_estimators()):
            if PROBLEM == 'IID':
                Estimator.plot('FIGS/RFGUnitTest'+str(update_num)+'_'+str(ind))
            if PROBLEM in ['LTR','FQI']:
                Estimator.test_results(update_num)
    FG.merge_logs()


"""
        if not os.path.isdir('FIGS'):
            os.makedirs('FIGS')
        for ind, Estimator in enumerate(FG.load_all_estimators()):
            Estimator.plot('FIGS/RFGUnitTest'+str(update_num)+'_'+str(ind))
"""









































""" TO BE COMPLETE: This is the Asynch version of NestedForestGrower meant to be run on a cluster


def AsynchNestedForetGrower_setup(DataGeneratorObj,
    NestedEstimatorModelClass=NRT.NestedDecisionEstimatorRegressor,
    NTMParams=None,
    base_dir='.',
    n_estimators=1,
    data_subsample_rate=.1):
    # Set up necessary folders
    folders = {}
    folders['todo'] = base_dir+'/TODO/'
    folders['claimed'] = base_dir+'/CLAIMED/'
    folders['done'] = base_dir+'/DONE/'
    folders['estimators'] = base_dir+'/ESTIMATORS/'
    folders['info'] = base_dir+'/INFO/'
    folders['old'] = base_dir+'/OLD/'
    folders['data'] = base_dir+'/DATA/'
    # Prepare to store disk write latencies
    from time import time
    Latencies = {'disk write':[]}
    # Create the folder keeping track of the latencies
    for D in self.folders.values():
        T0 = time()
        if not os.path.isdir(D):
            os.makedirs(D)
            Latencies['disk write'].append(5*(time()-T0))
    # Save the data generation object to the DATA folder
    save_obj(DataGeneratorObj,fname=folders['data']+'DataGenerator',COMPRESS_TEMP_FILES)
    self.estimators = []
    for ind in range(n_estimators):
        # Get data for each estimator
        Data = DataGeneratorObj.write_more_data_to_file(fname=folders['data']+'DataForEstimator'+str(ind)+'_0',data_subsample_rate=data_subsample_rate)
        # Create and save each estimator
        estimator = NestedEstimatorModelClass(param_values=NTMParams)
        self.estimators.append(estimator)
        T0 = time(); save_obj(estimator,fname=folders['estimators']+'NRT'+str(ind)); Latencies.append(time()-T0,COMPRESS_TEMP_FILES)
    LatencyInfo = {'disk write':10*max(Latencies)}
    save_obj(LatencyInfo,folders['info']+'Latencies',COMPRESS_TEMP_FILES)


class AsynchNestedForetGrower:
    def __init__(self,
                 DataGenerator,
                 NestedEstimatorModelClass=NRT.NestedDecisionEstimatorRegressor,
                 base_dir='.',
                 delay_range=[.1,.2]):
        # NestedEstimatorModelClass is assumed to have the following methods: get_potential_candidates(), temp_update_estimator(), delete_last_candidate(), full_update_estimator(), evaluate_for_comparison(), plot(), best_result(), save(), load()
        # DataGenerator is assumed to have the following methods: get_more_data(), write_more_data_to_file()
        self.Estimator = NestedEstimatorModelClass(param_values=NTMParams)
        self.DataGenerator = DataGenerator
        self.n_estimators = n_estimators
        self.max_features = max_features
        if max_features is None:
            self.max_features = "sqrt"
        self.data_subsample_rate = data_subsample_rate
        self.folders = {}
        self.folders['todo'] = base_dir+'/TODO/'
        self.folders['claimed'] = base_dir+'/CLAIMED/'
        self.folders['done'] = base_dir+'/DONE/'
        self.folders['estimators'] = base_dir+'/ESTIMATORS/'
        self.folders['info'] = base_dir+'/INFO/'
        self.folders['old'] = base_dir+'/OLD/'
        self.folders['data'] = base_dir+'/DATA/'
        for D in self.folders.values():
            T0 = time()
            if not os.path.isdir(D):
                os.makedirs(D)





"""

















































""" OLD METHODS:

    def full_evaluate_claimed_candidates(SNFG,JobID):
        # print "Method: RFG.full_evaluate_claimed_candidates()"
        Fs = [SNFG.folders['claimed']+f for f in os.listdir(SNFG.folders['claimed']) if '_JobID'+JobID in f]
        # Evaluate each potential candidate
        for f in Fs:
            oldf = f
            t_ind,s_ind,b_ind = fname2candidateInd(oldf)
            estimator = load_obj(oldf)
            # make a copy of the original forest grower object ...
            newSNFG = deepcopy(SNFG)
            # ... and fully add a 
            estimator['eval_result'] = newSNFG.full_evaluate_candidate(t_ind,estimator)
            newf = SNFG.folders['done']+candidateInd2fname(t_ind,s_ind,b_ind)+'_JobID'+JobID
            save_obj(estimator,newf,COMPRESS_TEMP_FILES)
            os.remove(oldf)

    def full_divide_tasks(SNFG,n_candidates):
        # print "Method: RFG.full_divide_tasks()"
        if SNFG.n_procs == 1:
            JID = SNFG.claim_candidates(n_candidates)
            full_evaluate_claimed_candidates(SNFG,JID)
        else:
            JIDs = []
            for ind in range(SNFG.n_procs):
                n_candidates_per_proc = ceil(n_candidates/SNFG.n_procs)
                JID = SNFG.claim_candidates(n_candidates_per_proc)
                JIDs.append(JID)
            from multiprocessing import Pool
            pool = Pool(processes=SNFG.n_procs)
            for ind in range(SNFG.n_procs):
                sleep(.1)
                pool.apply_async(SNFG.full_evaluate_claimed_candidates, (JIDs[ind],))
            pool.close()
            pool.join()  

"""
from numpy import *
set_printoptions(edgeitems=3, infstr='inf', linewidth=9999, nanstr='nan', precision=4, suppress=False, threshold=1000, formatter=None)
import sys
from numpy.random import randn, randint, choice, random_sample
import time
import cPickle as pkl
import gzip
import os, re
from copy import deepcopy


def save_obj(obj,fname):
    if not fname.endswith('.gzpkl'):
        fname += '.gzpkl'
    wh = gzip.open(fname,'w')
    pkl.dump(obj,wh)
    wh.close()

def load_obj(fname):
    if not fname.endswith('.gzpkl'):
        fname += '.gzpkl'
    fh = gzip.open(fname)
    obj = pkl.load(fh)
    fh.close()
    return obj

def diag_split(X):
    # Diagonal classifier
    return (X[:,0]>X[:,1])-.5

def tuples2dict(Pairs,Type='int'):
    # Pairs is assumed to be a list of binary tuples
    D = {}
    K, V = zip(*Pairs)
    for key in set(K):
        D[key] = array([v for k,v in Pairs if k == key]).astype(Type)
    return D

def list_argsort(L):
    LI = zip(L,range(len(L)))
    LI.sort()
    return zip(*LI)[1]

def is_iterable(obj):
    try:
        l = len(obj)
        return True
    except TypeError:
        return False


class xyData:
    def __init__(self,X,Y,sample_weight,sample_inds=None):
        # Feature vectors
        self.X = X
        # Target values
        self.Y = Y
        # Sample weights
        self.sample_weight = sample_weight
        # Indices of the data points on disk (NOT the indices in the xyData object!)
        self.sample_inds = sample_inds

    def sample_inds_as_list(self):
        if self.sample_inds.dtype == 'int':
            return self.sample_inds.tolist()
        else:
            return [tuple(si) for si in self.sample_inds]

    def __eq__(self,other):
        return (self.X==other.X).all() and (self.Y==other.Y).all() \
                and (self.sample_weight==other.sample_weight).all() \
                and (self.sample_inds==other.sample_inds).all() 

    def __getitem__(self,Inds):
        if self.sample_inds is None:
            new_sample_inds = None
        else:
            new_sample_inds = self.sample_inds[Inds]
        newData = xyData(self.X[Inds],
                          self.Y[Inds],
                          self.sample_weight[Inds],
                          new_sample_inds)
        return newData

    def __len__(self):
        return int(self.X.shape[0])

    def __add__(self,other):
        if len(other) == 0:
            return self.copy()
        if self.sample_inds is None and other.sample_inds is None:
            return xyData(X=r_[self.X,other.X],
                           Y=r_[self.Y,other.Y],
                           sample_weight=r_[self.sample_weight,other.sample_weight])
        elif self.sample_inds is not None and other.sample_inds is not None:
            return xyData(X=r_[self.X,other.X],
                           Y=r_[self.Y,other.Y],
                           sample_weight=r_[self.sample_weight,other.sample_weight],
                           sample_inds=r_[self.sample_inds,other.sample_inds])
        else:
            raise ValueError, 'ERROR: Both xyData objects need to have the same type of sample_inds variable!'

    def __iadd__(self,other):
        if len(other) == 0:
            return self
        self.X = r_[self.X,other.X]
        self.Y = r_[self.Y,other.Y]
        self.sample_weight = r_[self.sample_weight,other.sample_weight]
        if self.sample_inds is None and other.sample_inds is None:
            pass
        elif self.sample_inds is not None and other.sample_inds is not None:
            self.sample_inds = r_[self.sample_inds,other.sample_inds]
        else:
            raise ValueError, 'ERROR: Both xyData objects need to have the same type of sample_inds variable!'
        return self

    def __mul__(self,other):
        if type(other) == float or type(other) == int:
            return xaryData(X=self.X,
                            A=self.A,
                            R=self.R,
                            Y=self.Y,
                            sample_weight=other*self.sample_weight,
                            sample_inds=self.sample_inds)
        elif (self.sample_inds == other.sample_inds).all():
            return xaryData(X=self.X,
                            A=self.A,
                            R=self.R,
                            Y=self.Y,
                            sample_weight=self.sample_weight*other.sample_weight,
                            sample_inds=self.sample_inds)
        else:
            print "Inside DG.xyData.mul(): The second element in the multiplication should be either a number or xyData!"
            raise ValueError, "Inside DG.xyData.mul(): The second element in the multiplication should be either a number or xyData!"

    def copy(self):
        return xyData(self.X,
                       self.Y,
                       self.sample_weight,
                       self.sample_inds)

    def copy_values(self):
        return xyData(zeros(self.Y.shape),
                       self.Y,
                       self.sample_weight,
                       self.sample_inds)

    def __or__(self,other):
        if other is None:
            return self.copy()
        if self.sample_inds is None or other.sample_inds is None:
            raise ValueError, 'ERROR: You cannot take a union of two xyData objects if their sample_inds have not been set!'
        # Create a dictionary that maps each sample_ind to the pair (is_other,index): 
        # in particular, if a sample_ind appears in both self and other, dInds maps 
        # it to (1,ind), where ind is its index in other
        sInds = zip( self.sample_inds_as_list()  , zip(zeros(len(self),dtype='int'),arange(len(self))) ) \
              + zip( other.sample_inds_as_list() , zip(ones(len(other),dtype='int'),arange(len(other))) )
        sInds.sort()
        dInds = dict(sInds)
        # Sort the sample_inds
        SI = sorted(dInds.keys())
        # Get the (is_other,index) pairs in the same other as SI
        Inds = [dInds[si] for si in SI]
        # Convert the (is_other,index) pairs to the index of the concatenation of self and other
        Inds = array([o*len(self)+i for o,i in Inds])
        return (self+other)[Inds]

    def __ior__(self,other):
        return self | other

    def subsample(self,percentage,replace=True,return_inds=False):
        nSamples = len(self)
        Inds = range(nSamples)
        N = int(percentage*nSamples)
        chInds = choice(Inds,size=N,replace=replace)
        chInds.sort()
        chData = self[chInds]
        if return_inds:
            return chData,chInds
        else:
            return chData

    def dim(self):
        '''Return the dimension of the feature space, i.e. self.X'''
        if len(self.X.shape) > 1:
            return self.X.shape[1]
        else:
            return 1

    def __lt__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.xyData.__lt__(): You can only compare an xyData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.xyData.__lt__(): You can only compare an xyData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif other.keys()[0] >= self.dim():
            raise NotImplementedError('ERROR in xyData.__lt__(): You can only split a Query object along a valid feature!')
        f,th = other.items()[0]
        try:
            f = int(xy_f)
            th = float(th)
        except:
            raise ValueError('ERROR in DG.xyData.__lt__(): the feature and the threshold need to be convertible to an integer and a float!')
        return self.X[:,f] < th

    def __le__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.xyData.__le__(): You can only compare an xyData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.xyData.__le__(): You can only compare an xyData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif other.keys()[0] >= self.dim():
            raise NotImplementedError('ERROR in xyData.__le__(): You can only split a Query object along a valid feature!')
        f,th = other.items()[0]
        try:
            f = int(xy_f)
            th = float(th)
        except:
            raise ValueError('ERROR in DG.xyData.__le__(): the feature and the threshold need to be convertible to an integer and a float!')
        return self.X[:,f] <= th

    def __gt__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.xyData.__gt__(): You can only compare an xyData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.xyData.__gt__(): You can only compare an xyData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif other.keys()[0] >= self.dim():
            raise NotImplementedError('ERROR in xyData.__gt__(): You can only split a Query object along a valid feature!')
        f,th = other.items()[0]
        try:
            f = int(xy_f)
            th = float(th)
        except:
            raise ValueError('ERROR in DG.xyData.__gt__(): the feature and the threshold need to be convertible to an integer and a float!')
        return self.X[:,f] > th

    def __ge__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.xyData.__ge__(): You can only compare an xyData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.xyData.__ge__(): You can only compare an xyData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif other.keys()[0] >= self.dim():
            raise NotImplementedError('ERROR in xyData.__ge__(): You can only split a Query object along a valid feature!')
        f,th = other.items()[0]
        try:
            f = int(xy_f)
            th = float(th)
        except:
            raise ValueError('ERROR in DG.xyData.__ge__(): the feature and the threshold need to be convertible to an integer and a float!')
        return self.X[:,f] >= th

class iidDG:
    def __init__(self,n_samples=1000,func=diag_split,fname_prefix='',data_folder=''):
        # print "Method: iidDG.__init__()"
        self.n_samples = n_samples
        self.func = func
        self.data_folder = data_folder
        self.description = 'iidDG'
        self.fname_prefix = fname_prefix

    def fname2batch(self,fname):
        # Find the batch number of the data file
        return int(re.findall(self.data_folder+self.fname_prefix+self.description+r'(\d+)',fname)[0])

    def batch2fname(self,batch):
        # Convert batch number to file name
        return self.data_folder+self.fname_prefix+self.description+str(batch)

    def get_file_names(self):
        # print "Method: iidDG.get_file_names()"
        if self.data_folder is None:
            raise ValueError, 'ERROR in iidDG.get_file_names(): data_folder has not been set yet!'
        Fs = [self.data_folder+f for f in os.listdir(self.data_folder) if self.fname_prefix+self.description in f]
        Fs.sort(key=lambda f:(self.fname2batch(f),f))
        return Fs

    def get_last_data_batch(self):
        # print "Method: iidDG.get_last_data_batch()"
        Fs = self.get_file_names()
        f = Fs[-1]
        n = len(Fs)
        Data = load_obj(f)
        return Data

    def get_data_batch(self,n):
        # print "Method: iidDG.get_data_batch()"
        f = self.batch2fname(n)
        Data = load_obj(f)
        return Data

    def get_more_data(self):
        # print "Method: iidDG.get_more_data()"
        X = randn(self.n_samples,2)
        Y = self.func(X)
        SW = ones(self.n_samples)
        Data = xyData(X,Y,SW)
        return Data

    def write_more_data_to_file(self):
        # print "Method: iidDG.write_more_data_to_file()"
        data_batch = len(self.get_file_names())
        Data = self.get_more_data()
        Data.sample_inds = array([(data_batch,ind) for ind in range(len(Data))],dtype='i,i')
        fname = self.batch2fname(data_batch)
        save_obj(Data,fname)

    def read_data_from_file(self,fname):
        # print "Method: iidDG.read_data_from_file()"
        Data = load_obj(fname)
        return Data

    def __getitem__(self,sample_inds):
        # Given a list of sample indices, return a Data object with those samples

        ar_sample_inds = array(sample_inds,dtype='i,i')
        # First group the sample indices by batch
        dInds = tuples2dict(ar_sample_inds)
        # Inititalze the requested Data object to be empty
        Data = None
        # For each batch ...
        for batch in dInds:
            # get the data in that batch ...
            fullData = self.get_data_batch(batch)
            # and extract the data whose indices are requested.
            batchData = fullData[dInds[batch]]
            # If this is the first batch, set Data to be it ...
            if Data is None:
                Data = batchData.copy()
                if Data is None:
                    raise ValueError
            # and if not, add the data in the new batch to Data
            else:
                Data += batchData
                if Data is None:
                    raise ValueError
        # Finally, make sure the data in Data is in the same order as the original sample_inds
        initSI2sorted = ar_sample_inds.argsort()
        sorted2initSI = initSI2sorted.argsort()
        finSI2sorted = Data.sample_inds.argsort()
        Data = Data[finSI2sorted][sorted2initSI]
        if (Data.sample_inds != ar_sample_inds).any():
            raise ValueError, 'ERROR in iiDdG.__get__(): The sample_inds of the retrieved data should be the same as requested sample_inds!'
        return Data


class mmap_list:
    def __init__(self,fnames):
        # fnames - a list of file names containing arrays
        # NOTE: The arrays are assumed to have the same number of columns
        self.mmap_arrays = []
        self.row_indices = []
        self.fnames = fnames
        for f_ind, fname in enumerate(fnames):
            A = load(fname,mmap_mode='r')
            if f_ind == 0:
                self.col_indices = arange(A.shape[1] if len(A.shape)>1
                                                     else 1)
            else:
                if len(A.shape) > 1 and len(self.col_indices) != A.shape[1]:
                    raise ValueError("ERROR inside mmap_list.__init__(): You can only put memmap arrays with the same number of columns in one mmap_list!")
                if len(A.shape) > 2:
                    raise ValueError("ERROR inside mmap_list.__init__(): mmap_list only deals with lists of 1- or 2-dimensional arrays!")
            self.mmap_arrays.append(A)
            self.row_indices += [len(self.row_indices)+ind for ind in range(A.shape[0])]
        self.row_indices = array(self.row_indices)
        self.update_shape()
        self.dtype = self.mmap_arrays[0].dtype

    def update_shape(self):
        nR = len(self.row_indices)
        nC = len(self.col_indices)
        self.shape = (nR,nC)

    def num2array(self,obj):
        if is_iterable(obj):
            return obj
        else:
            return array([obj])

    def __getitem__(self,inds):
        r_inds = self.row_indices
        c_inds = self.col_indices
        other = deepcopy(self)
        if type(inds) != tuple:
            other.row_indices = self.num2array(r_inds[inds])
        elif len(inds) == 2:
            other.row_indices = self.num2array(r_inds[inds[0]])
            other.col_indices = self.num2array(c_inds[inds[1]])
        else:
            raise ValueError("ERROR inside mmap_list.__getitem__(): Invalid indices!")
        self.update_shape()
        other.update_shape()
        if other.shape[1] == 1 and other.shape[0] == self.shape[0]:
            # If a whole column is requested, then return the whole thing as an array. This is so, feature thresholding can be done more easily.
            return other.toarray().flatten()
        else:
            return other

    def get_array_row_inds(self):
        # For each index in self.row_indices, get the index of the mmap_array it belongs to and its index INSIDE that mmap_array
        n_array_row = array([0]+[A.shape[0] for A in self.mmap_arrays])
        n_array_start_inds = n_array_row.cumsum()
        full_row_inds = concatenate([ind*ones(A.shape[0],dtype=int) for ind,A in enumerate(self.mmap_arrays)])
        array_inds = full_row_inds[self.row_indices]
        array_ind_plus_row = array([(a_ind,r_ind-n_array_start_inds[a_ind],r_ind) for r_ind, a_ind in zip(self.row_indices,array_inds)],dtype='i,i,i')
        # Now group together consecutive pairs that have the same mmap_array index
        array_ind_changes_start = [0]+(1+nonzero(diff(array_inds))[0]).tolist()
        array_ind_changes_end = (1+nonzero(diff(array_inds))[0]).tolist()+[array_inds.shape[0]]
        if (array([array_ind_plus_row[ind][0] for ind in array_ind_changes_start]) 
            != array([array_ind_plus_row[ind-1][0] for ind in array_ind_changes_end])).any():
            raise ValueError("ERROR inside mmap_list.get_array_row_inds(): Something has gone horribly wrong! These two arrays should be exactly the same. DEBUG THIS!!")
        array_and_rows_inds = [( array_ind_plus_row[start][0],
                                array(zip(*array_ind_plus_row[start:end])[1]),
                                array(zip(*array_ind_plus_row[start:end])[2]) ) for start,end in zip(array_ind_changes_start,array_ind_changes_end)]
        return array_and_rows_inds

    def toarray(self):
        array_and_rows_inds = self.get_array_row_inds()
        c_inds = self.col_indices
        if not is_iterable(c_inds):
            raise ValueError("ERROR inside mmap_list.toarray(): the column indices should always be in a list, even if there is only one column!")
        a_ind, loc_r_inds, _ = array_and_rows_inds[0]
        if len(self.mmap_arrays[0].shape) == 1:
            return concatenate([self.mmap_arrays[a_ind][loc_r_inds] for a_ind, loc_r_inds, _ in array_and_rows_inds])
        else:
            return concatenate([self.mmap_arrays[a_ind][ix_(loc_r_inds,c_inds)] for a_ind, loc_r_inds, _ in array_and_rows_inds])

class xaryData:
    def __init__(self,X,A,R,Y,sample_weight,sample_inds=None,dXY=None):
        '''NOTE: this class assumes that X,A,R,Y are all array-type objects, e.g. numpy, memmap or mmap_list!'''
        # Initial states
        self.X = X
        # Actions
        self.A = A
        # Rewards
        self.R = R
        # Final state
        self.Y = Y
        if self.X[0].dtype != self.Y[0].dtype:
            raise ValueError("ERROR inside DG.xaryData.__init__(): All of the states need to have the same datatype!")
        # Difference in the initial and final states
        if dXY is None:
            self.dXY = Y-X
        else:
            self.dXY = dXY
        # Sample weights
        self.sample_weight = sample_weight
        # Indices of the data points on disk (NOT the indices in the xaryData object!)
        self.sample_inds = sample_inds if sample_inds is not None else None

    def sample_inds_as_list(self):
        if self.sample_inds.dtype == 'int':
            return self.sample_inds.tolist()
        else:
            return [tuple(si) for si in self.sample_inds]

    def __eq__(self,other):
        return (self.X==other.X).all() and (self.Y==other.Y).all() \
                and (self.A==other.A).all() and (self.R==other.R).all() \
                and (self.sample_weight==other.sample_weight).all() \
                and (self.sample_inds==other.sample_inds).all() 

    def __getitem__(self,Inds):
        if self.sample_inds is None:
            new_sample_inds = None
        else:
            new_sample_inds = self.sample_inds[Inds]
        newData = xaryData(self.X[Inds],
                          self.A[Inds],
                          self.R[Inds],
                          self.Y[Inds],
                          self.sample_weight[Inds],
                          new_sample_inds)
        return newData

    def __len__(self):
        return int(self.X.shape[0])

    def __add__(self,other):
        if len(other) == 0:
            return self.copy()
        if self.sample_inds is None and other.sample_inds is None:
            return xaryData(X=r_[self.X,other.X],
                            A=r_[self.A,other.A],
                            R=r_[self.R,other.R],
                            Y=r_[self.Y,other.Y],
                            sample_weight=r_[self.sample_weight,other.sample_weight])
        elif self.sample_inds is not None and other.sample_inds is not None:
            return xaryData(X=r_[self.X,other.X],
                            A=r_[self.A,other.A],
                            R=r_[self.R,other.R],
                            Y=r_[self.Y,other.Y],
                            sample_weight=r_[self.sample_weight,other.sample_weight],
                            sample_inds=r_[self.sample_inds,other.sample_inds])
        else:
            raise ValueError, 'ERROR: Both xaryData objects need to have the same type of sample_inds variable!'

    def __iadd__(self,other):
        if len(other) == 0:
            return self
        self.X = r_[self.X,other.X]
        self.A = r_[self.A,other.A]
        self.R = r_[self.R,other.R]
        self.Y = r_[self.Y,other.Y]
        self.sample_weight = r_[self.sample_weight,other.sample_weight]
        if self.sample_inds is None and other.sample_inds is None:
            pass
        elif self.sample_inds is not None and other.sample_inds is not None:
            self.sample_inds = r_[self.sample_inds,other.sample_inds]
        else:
            raise ValueError, 'ERROR: Both xaryData objects need to have the same type of sample_inds variable!'
        return self

    def __mul__(self,other):
        if type(other) == float or type(other) == int:
            return xaryData(X=self.X,
                            A=self.A,
                            R=self.R,
                            Y=self.Y,
                            sample_weight=other*self.sample_weight,
                            sample_inds=self.sample_inds)
        elif (self.sample_inds == other.sample_inds).all():
            return xaryData(X=self.X,
                            A=self.A,
                            R=self.R,
                            Y=self.Y,
                            sample_weight=self.sample_weight*other.sample_weight,
                            sample_inds=self.sample_inds)
        else:
            print "Inside DG.xaryData.mul(): The second element in the multiplication should be either a number or xaryData!"
            raise ValueError, "Inside DG.xaryData.mul(): The second element in the multiplication should be either a number or xaryData!"

    def copy(self):
        return xaryData(self.X,
                        self.A,
                        self.R,
                        self.Y,
                        self.sample_weight,
                        self.sample_inds)

    def copy_values(self):
        return xaryData(zeros(self.R.shape),
                        self.A,
                        self.R,
                        zeros(self.R.shape),
                        self.sample_weight,
                        self.sample_inds)

    def __or__(self,other):
        if other is None:
            return self.copy()
        if self.sample_inds is None or other.sample_inds is None:
            raise ValueError, 'ERROR: You cannot take a union of two xaryData objects if their sample_inds have not been set!'
        # Create a dictionary that maps each sample_ind to the pair (is_other,index): 
        # in particular, if a sample_ind appears in both self and other, dInds maps 
        # it to (1,ind), where ind is its index in other
        sInds = zip( self.sample_inds_as_list()  , zip(zeros(len(self),dtype='int'),arange(len(self))) ) \
              + zip( other.sample_inds_as_list() , zip(ones(len(other),dtype='int'),arange(len(other))) )
        sInds.sort()
        dInds = dict(sInds)
        # Sort the sample_inds
        SI = sorted(dInds.keys())
        # Get the (is_other,index) pairs in the same other as SI
        Inds = [dInds[si] for si in SI]
        # Convert the (is_other,index) pairs to the index of the concatenation of self and other
        Inds = array([o*len(self)+i for o,i in Inds])
        return (self+other)[Inds]

    def __ior__(self,other):
        return self | other

    def to_xyData(self):
        return xyData(X=c_[self.X,self.A],
                      Y=self.R,
                      sample_weight=self.sample_weight,
                      sample_inds=self.sample_inds)

    def subsample(self,percentage,replace=True,return_inds=False):
        nSamples = len(self)
        Inds = range(nSamples)
        N = int(percentage*nSamples)
        chInds = choice(Inds,size=N,replace=replace)
        chInds.sort()
        chData = self[chInds]
        if return_inds:
            return chData,chInds
        else:
            return chData

    def dim(self):
        '''Return the dimension of the state space, i.e. self.X'''
        if len(self.X.shape) > 1:
            return self.X.shape[1]
        else:
            return 1

    def __lt__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.xaryData.__lt__(): You can only compare an xaryData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.xaryData.__lt__(): You can only compare an xaryData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        xy_f,th = other.items()[0]
        if type(xy_f) == tuple:
            xy,f = xy_f
        else:
            xy = 'x'
            f = xy_f
        try:
            f = int(xy_f)
            th = float(th)
        except:
            raise ValueError('ERROR in DG.xaryData.__lt__(): the feature and the threshold need to be convertible to an integer and a float!')
        if f >= self.dim():
            raise NotImplementedError('ERROR in DG.xaryData.__lt__(): You can only compare an xaryData object along a valid feature!')
        if xy == 'x':
            return self.X[:,f] < th
        elif xy == 'y':
            return self.Y[:,f] < th
        else:
            raise ValueError('ERROR in DG.xaryData.__lt__(): You can split either along the source states or the target states, so '+str(xy)+' needs to be either x or y!')

    def __le__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.xaryData.__le__(): You can only compare an xaryData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.xaryData.__le__(): You can only compare an xaryData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        xy_f,th = other.items()[0]
        if type(xy_f) == tuple:
            xy,f = xy_f
        else:
            xy = 'x'
            f = xy_f
        try:
            f = int(xy_f)
            th = float(th)
        except:
            raise ValueError('ERROR in DG.xaryData.__le__(): the feature and the threshold need to be convertible to an integer and a float!')
        if f >= self.dim():
            raise NotImplementedError('ERROR in DG.xaryData.__le__(): You can only compare an xaryData object along a valid feature!')
        if xy == 'x':
            return self.X[:,f] <= th
        elif xy == 'y':
            return self.Y[:,f] <= th
        else:
            raise ValueError('ERROR in DG.xaryData.__le__(): You can split either along the source states or the target states, so '+str(xy)+' needs to be either x or y!')

    def __gt__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.xaryData.__gt__(): You can only compare an xaryData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.xaryData.__gt__(): You can only compare an xaryData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        xy_f,th = other.items()[0]
        if type(xy_f) == tuple:
            xy,f = xy_f
        else:
            xy = 'x'
            f = xy_f
        try:
            f = int(xy_f)
            th = float(th)
        except:
            raise ValueError('ERROR in DG.xaryData.__gt__(): the feature and the threshold need to be convertible to an integer and a float!')
        if f >= self.dim():
            raise NotImplementedError('ERROR in DG.xaryData.__gt__(): You can only compare an xaryData object along a valid feature!')
        if xy == 'x':
            return self.X[:,f] > th
        elif xy == 'y':
            return self.Y[:,f] > th
        else:
            raise ValueError('ERROR in DG.xaryData.__gt__(): You can split either along the source states or the target states, so '+str(xy)+' needs to be either x or y!')

    def __ge__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.xaryData.__ge__(): You can only compare an xaryData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.xaryData.__ge__(): You can only compare an xaryData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        xy_f,th = other.items()[0]
        if type(xy_f) == tuple:
            xy,f = xy_f
        else:
            xy = 'x'
            f = xy_f
        try:
            f = int(xy_f)
            th = float(th)
        except:
            raise ValueError('ERROR in DG.xaryData.__ge__(): the feature and the threshold need to be convertible to an integer and a float!')
        if f >= self.dim():
            raise NotImplementedError('ERROR in DG.xaryData.__ge__(): You can only compare an xaryData object along a valid feature!')
        if xy == 'x':
            return self.X[:,f] >= th
        elif xy == 'y':
            return self.Y[:,f] >= th
        else:
            raise ValueError('ERROR in DG.xaryData.__ge__(): You can split either along the source states or the target states, so '+str(xy)+' needs to be either x or y!')

    def save(self,folder):
        if not os.path.isdir(folder):
            os.makedirs(folder)
        # Assign the file names
        X_fname = os.path.join(folder,'X.npy')
        A_fname = os.path.join(folder,'A.npy')
        R_fname = os.path.join(folder,'R.npy')
        Y_fname = os.path.join(folder,'Y.npy')
        dXY_fname = os.path.join(folder,'dXY.npy')
        sample_weight_fname = os.path.join(folder,'sample_weight.npy')
        sample_inds_fname = os.path.join(folder,'sample_inds.npy')
        # Save each variable to its own file
        save(X_fname,self.X)
        save(A_fname,self.A)
        save(R_fname,self.R)
        save(Y_fname,self.Y)
        save(dXY_fname,self.dXY)
        save(sample_weight_fname,self.sample_weight)
        if self.sample_inds is not None:
            save(sample_inds_fname,self.sample_inds)

    @classmethod
    def load(cls,folders,chatty=False):
        if type(folders) == str:
            # Assign the file names
            X_fname = os.path.join(folders,'X.npy')
            A_fname = os.path.join(folders,'A.npy')
            R_fname = os.path.join(folders,'R.npy')
            Y_fname = os.path.join(folders,'Y.npy')
            dXY_fname = os.path.join(folders,'dXY.npy')
            sample_weight_fname = os.path.join(folders,'sample_weight.npy')
            sample_inds_fname = os.path.join(folders,'sample_inds.npy')
            # Load each variable from its own file
            X = load(X_fname,mmap_mode='r')
            A = load(A_fname,mmap_mode='r')
            R = load(R_fname,mmap_mode='r')
            Y = load(Y_fname,mmap_mode='r')
            dXY = load(dXY_fname,mmap_mode='r')
            sample_weight = load(sample_weight_fname,mmap_mode='r')
            if os.path.isfile(sample_inds_fname):
                sample_inds = load(sample_inds_fname,mmap_mode='r')
            else:
                sample_inds = None
        elif type(folders) == list:
            # Assign the file name lists
            in_folders = list(folders)
            folders = []
            for D in in_folders:
                Fs = [os.path.join(D,'X.npy'),
                      os.path.join(D,'A.npy'),
                      os.path.join(D,'R.npy'),
                      os.path.join(D,'Y.npy'),
                      os.path.join(D,'dXY.npy'),
                      os.path.join(D,'sample_weight.npy'),
                      os.path.join(D,'sample_inds.npy')]
                if array([os.path.isfile(f) for f in Fs]).all():
                    folders.append(D)
                else:
                    if chatty:
                        print "WARNING inside DG.xaryData.load(): Folder", D, "is empty!"
            X_fnames = [os.path.join(D,'X.npy') for D in folders]
            A_fnames = [os.path.join(D,'A.npy') for D in folders]
            R_fnames = [os.path.join(D,'R.npy') for D in folders]
            Y_fnames = [os.path.join(D,'Y.npy') for D in folders]
            dXY_fnames = [os.path.join(D,'dXY.npy') for D in folders]
            sample_weight_fnames = [os.path.join(D,'sample_weight.npy') for D in folders]
            sample_inds_fnames = [os.path.join(D,'sample_inds.npy') for D in folders]
            # Load each variable from its own files
            X = mmap_list(X_fnames)
            A = mmap_list(A_fnames)
            R = mmap_list(R_fnames)
            Y = mmap_list(Y_fnames)
            dXY = mmap_list(dXY_fnames)
            sample_weight = mmap_list(sample_weight_fnames)
            sample_inds = mmap_list(sample_inds_fnames)
        return xaryData(X=X,
                        A=A,
                        R=R,
                        Y=Y,
                        dXY=dXY,
                        sample_weight=sample_weight,
                        sample_inds=sample_inds)

class MDP:
    image_downscale_factor = 4
    def __init__(self,params=None):
        self.params = params
        if params is None or len(params) == 0:
            print "ERROR inside DG.MDP.init(): The MDP parameters cannot be empty!"
            raise ValueError
        else:
            self.name = params['name'] if 'name' in params else None
            # The state space: this is assumed to be a finite set of numbers, arrays OR tuples!
            #                  These are the only two data types that the xaryData class can handle.
            X = params['X'] if 'X' in params else None
            x0 = params['x0'] if 'x0' in params else None
            # The FINITE set of actions: these need to be consecutive numbers!
            A = params['A'] if 'A' in params else None
            reward_transition_func = params['reward_transition_func'] if 'reward_transition_func' in params else None
        if X is None:
            import gym
            gym.undo_logger_setup()
            try:
                self.env = gym.make(self.name)
            except:
                raise ValueError("ERROR inside DG.MDP.init(): Could not load environment "+self.name+"!")
            # Put the specifications of the environment in local variables:
            self.Project = False
            x = self.gym2me(self.env.reset())
            dim = prod(self.env.observation_space.shape)
            X = array([self.gym2me(self.env.observation_space.low),
                            self.gym2me(self.env.observation_space.high)])
            if 'dim' not in params or params['dim'] <= dim:
            # If the prescribed dimension of the state space is larger than or equal to the actual dimension, adopt these variables:
                self.x = x
                self.dim = dim
                self.X = X
                self.Project = False
            else:
            # Otherwise, do dimension reduction:
                self.Project = True
                self.ProjMat = mat(randn(int(params['dim']),dim))/sqrt(dim)
                self.dim = int(params['dim'])
                self.x = self.Pr(x)
                center = X.mean(axis=0)
                c = self.Pr(center)
                radius = abs(diff(X,axis=0))/2
                newX = zeros([2,self.dim])
                for ind in range(self.ProjMat.shape[0]):
                    p = self.ProjMat[ind].A.flatten()
                    newX[0,ind] = c[ind] + (sign(p) * radius).sum()
                    newX[1,ind] = c[ind] - (sign(p) * radius).sum()
                self.X = newX
            self.A = arange(self.env.action_space.n)
        else:
            self.env = None
            if type(X[0]) == tuple:
                ar_type = ','.join(['i' for _ in range(len(X[0]))])
                self.X = array(X,dtype=ar_type)
            else:
                self.X = array(X)
            self.dim = 1
            self.x0 = x0
            self.x = x0
            self.A = array(A)
            self.reward_transition_func = reward_transition_func

    def Pr(self,x):
        # Project to the lower dimensional space
        # x is assumed to be a 1D array
        if self.Project:
            return (self.ProjMat * mat(x).T).A.flatten()
        else:
            return x

    def gym2me(self,x):
        x_ = MDP.reduce_atari_image(x).flatten()
        if self.Project:
            x_ = self.Pr(x_)
        return x_

    def all_actions(self):
        return self.A

    def n_actions(self):
        self.A.shape[0]

    def rand_action(self):
        return choice(self.A)

    def rand_state(self):
        if self.env is not None:
            return self.X[0] + (self.X[1]-self.X[0]) * random_sample(self.X[0].shape)

        else:
            return choice(self.X)

    def reset(self):
        if self.env is not None:
            obs = self.env.reset()
            self.x = self.Pr(self.gym2me(obs))
        else:
            self.x = self.x0
        return self.x

    def state(self):
        # Return the current state
        return self.x

    def act(self,a):
        if self.env is not None:
            x = self.x
            obs, r, done, _ = self.env.step(a)
            y = self.Pr(self.gym2me(obs))
        else:
            r, y, done = self.reward_transition_func(self.x,a)
        if done:
            self.x = self.reset()
            r = 10000*(r-1)
        else:
            self.x = array(y)
        return r,y

    def copy_to_save(self):
        M = deepcopy(self)
        M.env = None
        return M

    def recover_from_saved(self):
        import gym
        try:
            self.env = gym.make(self.name)
        except:
            raise ValueError("ERROR inside DG.MDP.init(): Could not load environment "+self.name+"!")
        self.env.reset()

    def __str__(self):
        if self.env is not None:
            return self.env.spec.id
        else:
            return 'DiscreteMDPwithStateSize'+str(len(self.X))+'andActionSize'+str(len(self.A))

    def __getitem__(self,x):
        # This is just for generating random exploration data!
        return self.rand_action()

    @classmethod
    def reduce_atari_image(cls, im3):
        from skimage.color import rgb2gray
        from skimage.transform import pyramid_reduce
        try:
            if im3.shape != (210,160,3):
                return im3
        except AttributeError:
            return im3
        im = rgb2gray(im3)
        small_im = pyramid_reduce(im,downscale=cls.image_downscale_factor)
        return small_im.flatten()

class mdpDG:
    def __init__(self,n_samples=1000,mdp_params={'name':''},fname_prefix='',data_folder='',r_max=1):
        # print "Method: mdpDG.__init__()"
        self.n_samples = min(n_samples,5*10**5)
        self.mdp_name = mdp_params['name']
        self.mdp = MDP(params=mdp_params)
        self.description = 'mdpDG_'
        self.fname_prefix = fname_prefix
        self.r_max = r_max
        self.data_folder = os.path.join(data_folder,self.fname_prefix+self.description+self.mdp_name)
        if not os.path.isdir(self.data_folder):
            os.makedirs(self.data_folder)

    def get_actions(self):
        return self.mdp.all_actions()

    def folder2batch(self,folder):
        # Find the batch number of the data file
        # return int(re.findall(os.path.join(self.data_folder,self.fname_prefix+self.description+self.mdp_name,r'(\d+)'),folder)[0])
        return int(os.path.split(folder)[-1])

    def batch2folder(self,batch):
        # Convert batch number to file name
        return os.path.join(self.data_folder,str(batch))

    def get_folder_names(self):
        # print "Method: mdpDG.get_folder_names()"
        if self.data_folder is None:
            raise ValueError, 'ERROR in mdpDG.get_folder_names(): data_folder has not been set yet!'
        Ds = [os.path.join(self.data_folder,D) for D in os.listdir(self.data_folder) if os.path.isdir(os.path.join(self.data_folder,D))]
        Ds.sort(key=lambda D:(self.folder2batch(D),D))
        return Ds

    def get_last_data_batch(self):
        # print "Method: mdpDG.get_last_data_batch()"
        Ds = self.get_folder_names()
        D = Ds[-1]
        Data = xaryData.load(D)
        return Data

    def get_all_data(self,chatty=False):
        Ds = self.get_folder_names()
        if len(Ds) == 0:
            raise ValueError("ERROR inside DG.mdpDG.get_all_data(): There is no data to load!")
        allData = xaryData.load(Ds,chatty=chatty)
        return allData

    def get_data_batch(self,n):
        # print "Method: mdpDG.get_data_batch()"
        D = self.batch2folder(n)
        Data = xaryData.load(D)
        return Data

    def get_more_data(self,policy,add_rmax=False):
        # print "Method: mdpDG.get_more_data()"
        X,A,R,Y = [],[],[],[]
        for ind in range(self.n_samples):
            x = self.mdp.state()
            a = policy[x]
            r,y = self.mdp.act(a)
            X.append(x)
            A.append(a)
            R.append(r)
            Y.append(y)
            if type(x) != type(y):
                raise ValueError
        if add_rmax:
            for ind in range(self.n_samples/1000):
                x = self.mdp.rand_state()
                a = self.mdp.rand_action()
                X.append(x)
                A.append(a)
                R.append(self.r_max)
                Y.append(x)
        X = array(X)
        A = array(A)
        R = array(R)
        Y = array(Y)
        SW = ones(len(A))
        Data = xaryData(X,A,R,Y,SW)
        return Data

    def write_more_data_to_file(self,policy,chatty=False):
        # print "Method: mdpDG.write_more_data_to_file()"
        data_batch = len(self.get_folder_names())
        folder = self.batch2folder(data_batch)
        if not os.path.isdir(folder):
            os.makedirs(folder)
        Data = self.get_more_data(policy)
        Data.sample_inds = array([(data_batch,ind) for ind in range(len(Data))],dtype='i,i')
        if chatty:
            print "Saving data to", folder
        Data.save(folder)

    def read_data_from_file(self,folder):
        # print "Method: mdpDG.read_data_from_file()"
        Data = xaryData.load(folder)
        return Data

    def __getitem__(self,sample_inds):
        # Given a list of sample indices, return a Data object with those samples

        ar_sample_inds = array(sample_inds,dtype='i,i')
        # First group the sample indices by batch
        dInds = tuples2dict(ar_sample_inds)
        # Inititalze the requested Data object to be empty
        Data = None
        # For each batch ...
        for batch in dInds:
            # get the data in that batch ...
            fullData = self.get_data_batch(batch)
            # and extract the data whose indices are requested.
            batchData = fullData[dInds[batch]]
            # If this is the first batch, set Data to be it ...
            if Data is None:
                Data = batchData.copy()
                if Data is None:
                    raise ValueError
            # and if not, add the data in the new batch to Data
            else:
                Data += batchData
                if Data is None:
                    raise ValueError
        # Finally, make sure the data in Data is in the same order as the original sample_inds
        initSI2sorted = ar_sample_inds.argsort()
        sorted2initSI = initSI2sorted.argsort()
        finSI2sorted = Data.sample_inds.argsort()
        Data = Data[finSI2sorted][sorted2initSI]
        if (Data.sample_inds != ar_sample_inds).any():
            raise ValueError, 'ERROR in mdpdG.__get__(): The sample_inds of the retrieved data should be the same as requested sample_inds!'
        return Data

    def copy_to_save(self):
        DG = deepcopy(self)
        DG.mdp = self.mdp.copy_to_save()
        return DG

    def recover_from_saved(self):
        self.mdp.recover_from_saved()



class qfData:
    def __init__(self,feature_dim,
        query_features,
        training_validation_query_feature_values,
        training_validation_query_inds,
        test_query_feature_values,
        test_query_inds):
        self.feature_dim = feature_dim
        self.query_features = query_features
        self.training_validation_query_feature_values = training_validation_query_feature_values
        self.training_validation_query_inds = training_validation_query_inds
        self.test_query_feature_values = test_query_feature_values
        self.test_query_inds = test_query_inds

    def get_training_validation_values(self,expanded=False):
        if expanded:
            X = zeros([self.training_validation_query_feature_values.shape[0],self.feature_dim])
            X.T[self.query_features] = self.training_validation_query_feature_values.T
            return X
        else:
            return self.training_validation_query_feature_values

    def get_test_values(self,expanded=False):
        if expanded:
            X = zeros([self.test_query_feature_values.shape[0],self.feature_dim])
            X.T[self.query_features] = self.test_query_feature_values.T
            return X
        else:
            return self.test_query_feature_values

    def __getitem__(self,(training_validation_inds,test_inds)):
        return qfData(self.feature_dim,self.query_features,
                    training_validation_query_feature_values=self.training_validation_query_feature_values[training_validation_inds],
                    training_validation_query_inds=self.training_validation_query_inds[training_validation_inds],
                    test_query_feature_values=self.test_query_feature_values[test_inds],
                    test_query_inds=self.test_query_inds[test_inds])

    def __lt__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.qfData.__lt__(): You can only compare a qfData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.qfData.__lt__(): You can only compare a qfData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif other.keys()[0] not in self.query_features:
            raise NotImplementedError('ERROR in DG.qfData.__lt__(): You can only split a qfData object along a query feature!')
        f,th = other.items()[0]
        f = nonzero(self.query_features == f)[0][0]
        return (self.training_validation_query_feature_values[:,f] < th, 
                self.test_query_feature_values[:,f] < th)

    def __le__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.qfData.__le__(): You can only compare a qfData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.qfData.__le__(): You can only compare a qfData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif other.keys()[0] not in self.query_features:
            raise NotImplementedError('ERROR in DG.qfData.__le__(): You can only split a qfData object along a query feature!')
        f,th = other.items()[0]
        f = nonzero(self.query_features == f)[0][0]
        return (self.training_validation_query_feature_values[:,f] <= th, 
                self.test_query_feature_values[:,f] <= th)

    def __gt__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.qfData.__gt__(): You can only compare a qfData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.qfData.__gt__(): You can only compare a qfData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif other.keys()[0] not in self.query_features:
            raise NotImplementedError('ERROR in DG.qfData.__gt__(): You can only split a qfData object along a query feature!')
        f,th = other.items()[0]
        f = nonzero(self.query_features == f)[0][0]
        return (self.training_validation_query_feature_values[:,f] > th, 
                self.test_query_feature_values[:,f] > th)

    def __ge__(self,other):
        # This returns an array of indices where the condition is satisfied
        if type(other) != dict:
            raise NotImplementedError('ERROR in DG.qfData.__ge__(): You can only compare a qfData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif len(other) > 1:
            raise NotImplementedError('ERROR in DG.qfData.__ge__(): You can only compare a qfData object against '+
              'a dictionary object with a single element, whose key is the index of the feature being used and whose value is the threshold!')
        elif other.keys()[0] not in self.query_features:
            raise NotImplementedError('ERROR in DG.qfData.__ge__(): You can only split a qfData object along a query feature!')
        f,th = other.items()[0]
        f = nonzero(self.query_features == f)[0][0]
        return (self.training_validation_query_feature_values[:,f] >= th, 
                self.test_query_feature_values[:,f] >= th)

    def copy_values(self):
        return qfData(self.feature_dim,self.query_features,
                    training_validation_query_feature_values=self.training_validation_query_feature_values,
                    training_validation_query_inds=self.training_validation_query_inds,
                    test_query_feature_values=self.test_query_feature_values,
                    test_query_inds=self.test_query_inds)

    def __or__(self,other):
        # Training query inds and featyre values
        union_training_validation_dict = dict(zip(self.training_validation_query_inds,self.training_validation_query_feature_values))
        union_training_validation_dict.update(dict(zip(other.training_validation_query_inds,other.training_validation_query_feature_values)))
        union_training_validation_query_inds, union_training_validation_query_feature_values = zip(*sorted(union_training_validation_dict.items(),key=lambda t:t[0]))
        union_training_validation_query_feature_values = array(union_training_validation_query_feature_values)
        # Validation query inds and featyre values
        # Test query inds and featyre values
        union_test_dict = dict(zip(self.test_query_inds,self.test_query_feature_values))
        union_test_dict.update(dict(zip(other.test_query_inds,other.test_query_feature_values)))
        union_test_query_inds, union_test_query_feature_values = zip(*sorted(union_test_dict.items(),key=lambda t:t[0]))
        union_test_query_feature_values = array(union_test_query_feature_values)
        return qfData(self.feature_dim,self.query_features,
                    union_training_validation_query_feature_values, 
                    union_training_validation_query_inds,
                    union_test_query_feature_values, 
                    union_test_query_inds)

    def get_indices(self):
        return (self.training_validation_query_inds,
                self.test_query_inds)

    def query_counts(self):
        return self.training_validation_query_feature_values.shape[0], self.test_query_feature_values.shape[0]

class ltrDG:
    def __init__(self,data_dir='',fname_prefix='',train_fname='training',valid_fname='validation',test_fname='test',empty_object=False):
        from rankpy.queries import Queries
        from rankpy.queries import concatenate as concatenate_queries
        from rankpy.gridsearch import train_test_split
        if empty_object:
            self.training_validation_queries = None
            self.test_queries = None
        else:
            self.data_dir = data_dir
            try:
                training_queries = Queries.load(os.path.join(data_dir,'training'), mmap='r')
                validation_queries = Queries.load(os.path.join(data_dir,'validation'), mmap='r')
                self.training_validation_queries = concatenate_queries([training_queries,validation_queries])
                self.test_queries = Queries.load(os.path.join(data_dir,'test'), mmap='r')
            except NotImplementedError:
                raise ValueError('ERROR inside DG.ltrData.__init__(): You need to have the following files in the folder '
                                 +data_dir+': training, validation, test! Moreover, these file must be saved using Queries.save() with separate=True.')
            self.training_validation_queries.adjust(purge=True)
            self.qfData = self.get_query_features(is_original_data=True)

    def get_query_features(self,is_original_data=True,training_validation_query_inds=[],test_query_inds=[]):
        # print "Method: ltrDG.get_query_features()"
        from rankpy.queries import concatenate as concatenate_queries
        all_queries = concatenate_queries([self.training_validation_queries, self.test_queries])
        query_features,all_feature_values = all_queries.get_query_features_and_values()
        training_validation_query_feature_values = all_feature_values[:self.training_validation_queries.query_count()]
        test_query_feature_values = all_feature_values[-self.test_queries.query_count():]
        feature_dim = all_queries.feature_vectors.shape[1]
        if is_original_data:
            return qfData(feature_dim,
                        query_features,
                        training_validation_query_feature_values,
                        arange(training_validation_query_feature_values.shape[0]),
                        test_query_feature_values,
                        arange(test_query_feature_values.shape[0]))
        else:
            if ( training_validation_query_feature_values.shape[0] != len(training_validation_query_inds)
                or test_query_feature_values.shape[0] != len(test_query_inds) ):
                raise ValueError('ERROR inside DG.ltrDG.get_query_features(): The query indices need to be of the same size as the query feature values!')
            return qfData(feature_dim,
                        query_features,
                        training_validation_query_feature_values,
                        training_validation_query_inds,
                        test_query_feature_values,
                        test_query_inds)

    def __getitem__(self,(training_validation_inds,test_inds)):
        DG = ltrDG(empty_object=True)
        DG.data_dir = self.data_dir
        DG.training_validation_queries = self.training_validation_queries[training_validation_inds]
        DG.test_queries = self.test_queries[test_inds]
        DG.qfData = self.qfData[(training_validation_inds,test_inds)]
        return DG

    def query_counts(self):
        return self.training_validation_queries.query_count(), self.test_queries.query_count()

    def training_validation_query_count(self):
        return self.training_validation_queries.query_count()

    def test_query_count(self):
        return self.test_queries.query_count()

    def training_data(self,portion=.7):
        # print "Method: ltrDG.training_data()"
        N = self.training_validation_queries.query_count()
        Inds = range(N)
        chInds = sorted(choice(Inds,size=int(portion*N),replace=False))
        return self.training_validation_queries[chInds]

    def validation_data(self,portion=.1):
        # print "Method: ltrDG.validation_data()"
        N = self.training_validation_queries.query_count()
        Inds = range(N)
        chInds = sorted(choice(Inds,size=int(portion*N),replace=False))
        return self.training_validation_queries[chInds]

    def estop_data(self,portion=.1):
        # print "Method: ltrDG.estop_data()"
        N = self.training_validation_queries.query_count()
        Inds = range(N)
        chInds = sorted(choice(Inds,size=int(portion*N),replace=False))
        return self.training_validation_queries[chInds]

    def train_validation_data(self,train_portion=.7,estop_portion=.1,valid_portion=.1,test_portion=.1):
        # print "Method: ltrDG.validation_data()"
        tot_portions = train_portion + valid_portion + estop_portion + test_portion
        if tot_portions <= 0 or min(train_portion, valid_portion, estop_portion, test_portion) < 0:
            raise ValueError("ERROR inside ltrDG.train_validation_data(): You cannot request negative portions of the data! "+
                "In other words, none of these numbers should be negative and at least one of them should be strictly positive: "+
                "train_portion, valid_portion, estop_portion, test_portion, tot_portions = "+
                str((train_portion, valid_portion, estop_portion, test_portion, tot_portions)))
        if tot_portions > 1:
            print("WARNING inside ltrDG.train_validation_data(): the portions of training, validation and estopping queries add up to "+
                str(train_portion+valid_portion+estop_portion+test_portion)+", which is larger than 1.")
            train_portion = 1. * train_portion / tot_portions
            estop_portion = 1. * estop_portion / tot_portions
            valid_portion = 1. * valid_portion / tot_portions
            test_portion = 1. * test_portion / tot_portions
        totN = self.training_validation_queries.query_count()
        trainN = int(train_portion * totN)
        estopN = int(estop_portion * totN)
        validN = int(valid_portion * totN)
        testN = int(test_portion * totN)
        restInds = range(totN)
        trainInds = sorted(choice(restInds,size=trainN,replace=False)) if trainN > 0 else []
        restInds = sorted(set(restInds)-set(trainInds))
        estopInds = sorted(choice(restInds,size=estopN,replace=False)) if estopN > 0 else []
        restInds = sorted(set(restInds)-set(estopInds))
        validInds = sorted(choice(restInds,size=validN,replace=False)) if validN > 0 else []
        restInds = sorted(set(restInds)-set(validInds))
        testInds = sorted(choice(restInds,size=testN,replace=False)) if testN > 0 else []
        restInds = sorted(set(restInds)-set(testInds))
        return (self.training_validation_queries[trainInds] if trainN > 0 else None,
                self.training_validation_queries[estopInds] if estopN > 0 else None,
                self.training_validation_queries[validInds] if validN > 0 else None,
                self.training_validation_queries[testInds] if testN > 0 else None)

    def test_data(self):
        # print "Method: ltrDG.test_data()"
        return self.test_queries

    def split(self,feature,threshold):
        if feature not in self.qfData.query_features:
            raise ValueError("ERROR inside DG.ltrDG.split(): You can only split along a query feature!")
        leInds = self.qfData <= {feature:threshold}
        gtInds = self.qfData > {feature:threshold}
        return self[leInds], self[gtInds]

    def copy_to_save(self):
        DG = ltrDG(empty_object=True)
        DG.data_dir = self.data_dir
        DG.training_validation_queries = deepcopy(self.training_validation_queries)
        DG.training_validation_queries.feature_vectors = [os.path.join(self.data_dir,'training.feature_vectors.npy'),
                                                          os.path.join(self.data_dir,'validation.feature_vectors.npy')]
        DG.test_queries = deepcopy(self.test_queries)
        DG.test_queries.feature_vectors = os.path.join(self.data_dir,'test.feature_vectors.npy')
        DG.qfData = self.qfData.copy_values()
        if DG.training_validation_queries.feature_vectors is None or DG.test_queries.feature_vectors is None:
            raise ValueError("ERROR inside ltrDG.copy_values(): These variables are NOT supposed to be None! But, string.")
        return DG

    def recover_from_saved(self):
        if type(self.training_validation_queries.feature_vectors) == list:
            self.training_validation_queries.feature_vectors = vstack([load(fname, mmap_mode='r') for fname in self.training_validation_queries.feature_vectors])
        if type(self.test_queries.feature_vectors) == str:
            self.test_queries.feature_vectors = load(self.test_queries.feature_vectors, mmap_mode='r')
        if self.training_validation_queries.feature_vectors is None or self.test_queries.feature_vectors is None:
            raise ValueError("ERROR inside ltrDG.recover_feature_vectors(): These variables are NOT supposed to be None! But, memmap arrays.")




if __name__ == "__main__":
    """ TESTING medDG """
    MDPparams = {'name':sys.argv[1]}
    if not os.path.isdir('DATA'):
        os.makedirs('DATA')
    DG = mdpDG(n_samples=int(sys.argv[2]),mdp_params=MDPparams,fname_prefix='RAND_EXPLORE__',data_folder='DATA',r_max=1)
    for ind in range(10):
        print ind
        DG.write_more_data_to_file(DG.mdp,chatty=True)
        Data = DG.get_all_data(chatty=True)
        print "Data.X.shape =", Data.X.shape























""" OLD TEST CODE
    Data1 = xyData(randn(100,2),randn(100),ones(100),arange(100))
    Data2 = Data1[arange(50)]
    print 'len(Data1) =',len(Data1)
    print 'len(Data2) =',len(Data2)
    print 'len(Data1+Data2) =',len(Data1+Data2)
    print 'len(Data1|Data2) =',len(Data1|Data2)
    print 'Is Data1 == Data1|Data2?',Data1 == (Data1|Data2)

    DG = iidDG(fname_prefix='DGUnitTest')
    DG.set_data_folder('./DATA/')
    for ind in range(10):
        DG.write_more_data_to_file()
    sample_inds = [(ind,randint(1000)) for ind in range(10) for _ in range(5)]
    Data = DG[sample_inds]
    for i,(batch,ind) in enumerate(sample_inds):
        f = DG.batch2fname(batch)
        batchData = load_obj(f)
        if (Data.X[i] != batchData.X[ind]).any():
            raise ValueError, 'ERROR in DG: Something has gone wrong!'
"""

""" TESTING multiple inputs for __getitem__():
    class C:
        def __init__(self,L):
            self.L2 = [[(i,j) for j in L] for i in L]

        def __getitem__(self,(i,j)):
            return self.L2[i][j]

    o = C(range(10))
    print o[(2,3)]
"""

""" OLD METHODS
    def union(self,D1,D2):
        # Combine two datasets
        D12 = {'X':r_[D1['X'],D2['X']],
               'Y':r_[D1['Y'],D2['Y']],
               'sample_weight':r_[D1['sample_weight'],D2['sample_weight']],
               'sample_inds':r_[D1['sample_inds'],D2['sample_inds']]
               }
        return D12

    def slice_data(self,Data,Inds):
        indsData = Data[Inds]
        return indsData

    def subsample_training_data(self,train_portion=.1,get_estopping=True):
        from rankpy.gridsearch import train_test_split
        train, rest = train_test_split(self.training_validation_queries,test_size=1.-train_portion)
        _, estop_valid = train_test_split(rest,test_size=train_portion)
        if get_estopping:
            estop, valid = train_test_split(estop_valid,test_size=0.5)
            return train, estop, valid
        else:
            return train, estop_valid
"""
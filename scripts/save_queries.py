import numpy as np

import logging
import os
import sys

from rankpy.queries import Queries

from rankpy.models import LambdaMART

# Turn on logging.
logging.basicConfig(format='%(asctime)s : %(message)s', level=logging.INFO)

def load_and_save(DS,fname):
    dDS2nF = {'istella-letor':220, 'MSLR':136, 'NP2004':64, 'YandexLR':245, 'YLR1':700, 'YLR2':700}
    dN2N = {'train':'training','vali':'validation','test':'test'}
    # Load the query datasets.
    nFeatures = dDS2nF[DS.split('/')[0]]
    queries = Queries.load_from_text(os.environ['HOME']+'/online-learning-data/'+DS+'/'+fname+'.txt.gz', min_feature=1, max_feature=nFeatures)
    logging.info('=' * 80)
    # Save them to binary format ...
    print "Saving to", os.environ['HOME']+'/online-learning-data/'+DS+'/'+dN2N[fname]
    queries.save(os.environ['HOME']+'/online-learning-data/'+DS+'/'+dN2N[fname],separate=True)


def DS2nFeatures(DS): 
    dDS2nF = {'istella-letor':220, 'MSLR':136, 'NP2004':64, 'YandexLR':245, 'YLR1':700, 'YLR2':700}
    return dDS2nF[DS.split('/')[0]]



if __name__ == "__main__":
    from multiprocessing import Pool
    from time import sleep
    n_procs = 4
    if n_procs > 1:
        pool = Pool(processes=n_procs)
    for dataset in ['istella-letor', 'YLR1', 'YLR2', 'MSLR']:
      for fname in ['train','vali','test']:
        print "Starting with", dataset+'/'+fname+'.txt.gz'
        if n_procs > 1:
            pool.apply_async(load_and_save, (dataset,fname))
        else:
            load_and_save(dataset,fname)
        sleep(1)
    if n_procs > 1:
        pool.close()
        pool.join()

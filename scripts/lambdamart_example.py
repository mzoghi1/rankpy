#!/usr/bin/python2
# -*- coding: utf-8 -*-

import numpy as np

import logging
import os

from rankpy.queries import Queries
from rankpy.queries import find_constant_features, find_nonconstant_query_features

from rankpy.models import LambdaMART


# Turn on logging.
logging.basicConfig(format='%(asctime)s : %(message)s', level=logging.INFO)

dataset = 'YLR1'
if not os.path.isfile(os.environ['HOME']+'/online-learning-data/'+dataset+'/training') or \
   not os.path.isfile(os.environ['HOME']+'/online-learning-data/'+dataset+'/validation') or \
   not os.path.isfile(os.environ['HOME']+'/online-learning-data/'+dataset+'/test'):
    # Load the query datasets.
    training_queries = Queries.load_from_text(os.environ['HOME']+'/online-learning-data/'+dataset+'/train.txt.gz')
    validation_queries = Queries.load_from_text(os.environ['HOME']+'/online-learning-data/'+dataset+'/vali.txt.gz')
    test_queries = Queries.load_from_text(os.environ['HOME']+'/online-learning-data/'+dataset+'/test.txt.gz')
    logging.info('=' * 80)
    # Save them to binary format ...
    training_queries.save(os.environ['HOME']+'/online-learning-data/'+dataset+'/training',separate=True)
    validation_queries.save(os.environ['HOME']+'/online-learning-data/'+dataset+'/validation',separate=True)
    test_queries.save(os.environ['HOME']+'/online-learning-data/'+dataset+'/test',separate=True)

# ... because loading them will be then faster.
training_queries = Queries.load(os.environ['HOME']+'/online-learning-data/'+dataset+'/training', mmap='r')
validation_queries = Queries.load(os.environ['HOME']+'/online-learning-data/'+dataset+'/validation', mmap='r')
test_queries = Queries.load(os.environ['HOME']+'/online-learning-data/'+dataset+'/test', mmap='r')

logging.info('=' * 80)

# Print basic info about query datasets.
logging.info('Train queries: %s' % training_queries)
logging.info('Valid queries: %s' % validation_queries)
logging.info('Test queries: %s' %test_queries)

logging.info('=' * 80)

# Set this to True in order to remove queries containing all documents
# of the same relevance score -- these are useless for LambdaMART.
remove_useless_queries = False

# Find constant query-document features.
cfs = None
# find_constant_features([training_queries,
#                               validation_queries,
#                               test_queries])

# Get rid of constant features and (possibly) remove useless queries.
training_queries.adjust(remove_features=cfs, purge=remove_useless_queries)
validation_queries.adjust(remove_features=cfs, purge=remove_useless_queries)
test_queries.adjust(remove_features=cfs)

# Print basic info about query datasets.
logging.info('Train queries: %s' % training_queries)
logging.info('Valid queries: %s' % validation_queries)
logging.info('Test queries: %s' % test_queries)

logging.info('=' * 80)

model = LambdaMART(metric='nDCG@10', max_leaf_nodes=64, shrinkage=0.1,
                   estopping=50, n_jobs=-1, min_samples_leaf=50,
                   random_state=42)

model.fit(training_queries, validation_queries=validation_queries)

logging.info('=' * 80)

logging.info('%s on the test queries: %.8f'
             % (model.metric, model.evaluate(test_queries, n_jobs=-1)))

model.save('LambdaMART_L7_S0.1_E50_' + model.metric)
